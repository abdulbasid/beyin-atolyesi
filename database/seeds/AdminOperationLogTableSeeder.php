<?php

use Illuminate\Database\Seeder;

class AdminOperationLogTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_operation_log')->delete();
        
        \DB::table('admin_operation_log')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'path' => 'admin',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:26:02',
                'updated_at' => '2020-01-16 20:26:02',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 1,
                'path' => 'admin',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:26:35',
                'updated_at' => '2020-01-16 20:26:35',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '{"_pjax":"#pjax-container"}',
                'created_at' => '2020-01-16 20:38:21',
                'updated_at' => '2020-01-16 20:38:21',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"Slider","icon":"fa-bars","uri":"sliders","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:38:47',
                'updated_at' => '2020-01-16 20:38:47',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:38:47',
                'updated_at' => '2020-01-16 20:38:47',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"Hakk\\u0131m\\u0131zda","icon":"fa-bars","uri":null,"roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:38:55',
                'updated_at' => '2020-01-16 20:38:55',
            ),
            6 => 
            array (
                'id' => 7,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:38:55',
                'updated_at' => '2020-01-16 20:38:55',
            ),
            7 => 
            array (
                'id' => 8,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"9","title":"Hakk\\u0131m\\u0131zda","icon":"fa-bars","uri":"aboutuses","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:39:16',
                'updated_at' => '2020-01-16 20:39:16',
            ),
            8 => 
            array (
                'id' => 9,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:39:17',
                'updated_at' => '2020-01-16 20:39:17',
            ),
            9 => 
            array (
                'id' => 10,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"9","title":"Nas\\u0131l Yapar\\u0131z?","icon":"fa-bars","uri":"howdoes","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:39:39',
                'updated_at' => '2020-01-16 20:39:39',
            ),
            10 => 
            array (
                'id' => 11,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:39:39',
                'updated_at' => '2020-01-16 20:39:39',
            ),
            11 => 
            array (
                'id' => 12,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"Referanslar","icon":"fa-bars","uri":null,"roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:39:57',
                'updated_at' => '2020-01-16 20:39:57',
            ),
            12 => 
            array (
                'id' => 13,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:39:58',
                'updated_at' => '2020-01-16 20:39:58',
            ),
            13 => 
            array (
                'id' => 14,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"12","title":"Kategoriler","icon":"fa-bars","uri":"refcategories","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:40:25',
                'updated_at' => '2020-01-16 20:40:25',
            ),
            14 => 
            array (
                'id' => 15,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:40:25',
                'updated_at' => '2020-01-16 20:40:25',
            ),
            15 => 
            array (
                'id' => 16,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"12","title":"Referanslar","icon":"fa-bars","uri":"referances","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:40:48',
                'updated_at' => '2020-01-16 20:40:48',
            ),
            16 => 
            array (
                'id' => 17,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:40:49',
                'updated_at' => '2020-01-16 20:40:49',
            ),
            17 => 
            array (
                'id' => 18,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"\\u0130\\u015f Ortaklar\\u0131","icon":"fa-bars","uri":"partners","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:41:33',
                'updated_at' => '2020-01-16 20:41:33',
            ),
            18 => 
            array (
                'id' => 19,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:41:33',
                'updated_at' => '2020-01-16 20:41:33',
            ),
            19 => 
            array (
                'id' => 20,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"Neler Yapar\\u0131z?","icon":"fa-bars","uri":"whatwedoess","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:41:58',
                'updated_at' => '2020-01-16 20:41:58',
            ),
            20 => 
            array (
                'id' => 21,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:41:58',
                'updated_at' => '2020-01-16 20:41:58',
            ),
            21 => 
            array (
                'id' => 22,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"Ekibimiz","icon":"fa-bars","uri":"teams","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:42:10',
                'updated_at' => '2020-01-16 20:42:10',
            ),
            22 => 
            array (
                'id' => 23,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:42:10',
                'updated_at' => '2020-01-16 20:42:10',
            ),
            23 => 
            array (
                'id' => 24,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"Makaleler","icon":"fa-bars","uri":"articless","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:42:24',
                'updated_at' => '2020-01-16 20:42:24',
            ),
            24 => 
            array (
                'id' => 25,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:42:24',
                'updated_at' => '2020-01-16 20:42:24',
            ),
            25 => 
            array (
                'id' => 26,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"parent_id":"0","title":"ileti\\u015fim","icon":"fa-bars","uri":"contacts","roles":["1",null],"permission":"*","_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn"}',
                'created_at' => '2020-01-16 20:42:43',
                'updated_at' => '2020-01-16 20:42:43',
            ),
            26 => 
            array (
                'id' => 27,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '[]',
                'created_at' => '2020-01-16 20:42:43',
                'updated_at' => '2020-01-16 20:42:43',
            ),
            27 => 
            array (
                'id' => 28,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'POST',
                'ip' => '::1',
                'input' => '{"_token":"rG98GTLJkRNtAHih6Evu1kGVnEOdCh3K94Pb9aZn","_order":"[{\\"id\\":8},{\\"id\\":9,\\"children\\":[{\\"id\\":10},{\\"id\\":11}]},{\\"id\\":12,\\"children\\":[{\\"id\\":13},{\\"id\\":14}]},{\\"id\\":15},{\\"id\\":16},{\\"id\\":17},{\\"id\\":18},{\\"id\\":19},{\\"id\\":1},{\\"id\\":2,\\"children\\":[{\\"id\\":3},{\\"id\\":4},{\\"id\\":5},{\\"id\\":6},{\\"id\\":7}]}]"}',
                'created_at' => '2020-01-16 20:42:56',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            28 => 
            array (
                'id' => 29,
                'user_id' => 1,
                'path' => 'admin/auth/menu',
                'method' => 'GET',
                'ip' => '::1',
                'input' => '{"_pjax":"#pjax-container"}',
                'created_at' => '2020-01-16 20:42:56',
                'updated_at' => '2020-01-16 20:42:56',
            ),
        ));
        
        
    }
}