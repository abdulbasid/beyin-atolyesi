<?php

use Illuminate\Database\Seeder;

class DivingtraningsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('divingtranings')->delete();
        
        \DB::table('divingtranings')->insert(array (
            0 => 
            array (
                'id' => 1,
                'main_title' => 'TSSF CMAS EĞİTİMLERİ',
                'slug' => 'tssf-cmas-egitimleri',
                'title' => 'TSSF CMAS EĞİTİMLERİ',
                'desciription' => '<p>TSSF CMAS EĞİTİMLERİ</p>',
                'file' => NULL,
                'created_at' => '2019-11-27 05:01:29',
                'updated_at' => '2019-12-01 21:13:27',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'main_title' => 'PADI EĞİTİMLERİ',
                'slug' => 'padi-egitimleri',
                'title' => 'PADI EĞİTİMLERİ',
                'desciription' => '<p>PADI EĞİTİMLERİ</p>',
                'file' => NULL,
                'created_at' => '2019-11-27 05:03:32',
                'updated_at' => '2019-12-01 21:14:09',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'main_title' => 'SSI EĞİTİMLERİ',
                'slug' => 'ssi-egitimleri',
                'title' => 'SSI EĞİTİMLERİ',
                'desciription' => '<p>SSI EĞİTİMLERİ</p>',
                'file' => NULL,
                'created_at' => '2019-11-27 05:03:43',
                'updated_at' => '2019-12-01 21:14:27',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}