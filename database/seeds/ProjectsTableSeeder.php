<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('projects')->delete();
        
        \DB::table('projects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Mars Media',
                'slug' => 'mars-media',
                'desciription' => 'Mars Media',
                'file' => 'http://localhost/laboratuar/public/image/zIFoafJdo2T2TLEyPlg16ATqGEKoqFw4aWvLfLuI.png',
                'created_at' => '2019-11-21 17:00:36',
                'updated_at' => '2019-11-21 17:00:37',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'POWER GROUP',
                'slug' => 'power-group',
                'desciription' => 'POWER GROUP',
                'file' => 'http://localhost/laboratuar/public/image/8aYwXm7Tyl0xT7ARn661YgaYmXohnKisOfOv8a5b.png',
                'created_at' => '2019-11-21 17:01:01',
                'updated_at' => '2019-11-21 17:01:01',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'KARNAVAL MEDIA GROUP',
                'slug' => 'karnaval-media-group',
                'desciription' => 'KARNAVAL MEDIA GROUP',
                'file' => 'http://localhost/laboratuar/public/image/McfLY8ZN0v2tROX59mQ8Ylwfi3yo7YD4AB1867hm.png',
                'created_at' => '2019-11-21 17:01:19',
                'updated_at' => '2019-11-21 17:01:19',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'KAFA RADİO',
                'slug' => 'kafa-radio',
                'desciription' => 'KAFA RADİO',
                'file' => 'http://localhost/laboratuar/public/image/2Bv4fO7wEiBytgK9Zrvoea9vnmoQfbWitTBRPexa.png',
                'created_at' => '2019-11-21 17:01:38',
                'updated_at' => '2019-11-21 17:01:38',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'SLOW TÜRK',
                'slug' => 'slow-turk',
                'desciription' => 'SLOW TÜRK',
                'file' => 'http://localhost/laboratuar/public/image/Mgt7yoJ3RCuGOnRnIA6gtoHEImxiGTiAO5UtrkUB.png',
                'created_at' => '2019-11-21 17:02:05',
                'updated_at' => '2019-11-21 17:02:05',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'RADYO 104',
                'slug' => 'radyo-104',
                'desciription' => 'RADYO 104',
                'file' => 'http://localhost/laboratuar/public/image/zJ5IWHKkSeaqFPDLBcwUhR1Co4LdToU3C4b0l1bF.png',
                'created_at' => '2019-11-21 17:02:30',
                'updated_at' => '2019-11-21 17:02:30',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'CNN TÜRK',
                'slug' => 'cnn-turk',
                'desciription' => 'CNN TÜRK',
                'file' => 'http://localhost/laboratuar/public/image/zbRPLIVZGlEzuh3WEpwYNoTzjZrAzeTGrHTJaMBL.png',
                'created_at' => '2019-11-21 17:02:46',
                'updated_at' => '2019-11-21 17:02:46',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'KRAL FM',
                'slug' => 'kral-fm',
                'desciription' => 'KRAL FM',
                'file' => 'http://localhost/laboratuar/public/image/BQtRBuUlQFqpzFD2IfZTI3E9ImhkkP0sew7SG6pc.png',
                'created_at' => '2019-11-21 17:03:03',
                'updated_at' => '2019-11-21 17:03:03',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'KRAL POP',
                'slug' => 'kral-pop',
                'desciription' => 'KRAL POP',
                'file' => 'http://localhost/laboratuar/public/image/qUdeXZ4k3PRDrFgVmlVRKGrDz4oonmjirgK0C9tI.png',
                'created_at' => '2019-11-21 17:03:18',
                'updated_at' => '2019-11-21 17:03:18',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'TURKUVAZ MEDYA GRUBU',
                'slug' => 'turkuvaz-medya-grubu',
                'desciription' => 'TURKUVAZ MEDYA GRUBU',
                'file' => 'http://localhost/laboratuar/public/image/EjAMdwZzSTuXIQ9DotedWhOQgh5qrg0zuNfE8uCk.png',
                'created_at' => '2019-11-21 17:03:44',
                'updated_at' => '2019-11-21 17:03:44',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'ALEM FM 89.2',
                'slug' => 'alem-fm-89-2',
                'desciription' => 'ALEM FM 89.2',
                'file' => 'http://localhost/laboratuar/public/image/3doapxrYrTikYsQSc4fcPMw1CyFbkFrFeTuQQyuq.png',
                'created_at' => '2019-11-21 17:04:04',
                'updated_at' => '2019-11-21 17:04:04',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'LİG RADİO',
                'slug' => 'lig-radio',
                'desciription' => 'LİG RADİO',
                'file' => 'http://localhost/laboratuar/public/image/I27RPopjMugXzcFQjdD67KHVwf1fANw3S9jWY4iH.png',
                'created_at' => '2019-11-21 17:04:23',
                'updated_at' => '2019-11-21 17:04:23',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'RADYO ALATURKA',
                'slug' => 'radyo-alaturka',
                'desciription' => 'RADYO ALATURKA',
                'file' => 'http://localhost/laboratuar/public/image/X03xdGAC3FMHSy19tAIk8ROke7U6Uyd22JdTSvJv.png',
                'created_at' => '2019-11-21 17:04:44',
                'updated_at' => '2019-11-21 17:04:44',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'RADYO SEYMEN',
                'slug' => 'radyo-seymen',
                'desciription' => 'RADYO SEYMEN',
                'file' => 'http://localhost/laboratuar/public/image/JCzSvZ9LA8D8ijQNiCyoDcrt7dJq8hC5CKY37ny6.png',
                'created_at' => '2019-11-21 17:05:08',
                'updated_at' => '2019-11-21 17:05:08',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'RADYO VİVA',
                'slug' => 'radyo-viva',
                'desciription' => 'RADYO VİVA',
                'file' => 'http://localhost/laboratuar/public/image/esbPyEgKX2jn4aWQ53GBanoH5IL3S9lgCbuRlgDz.png',
                'created_at' => '2019-11-21 17:05:30',
                'updated_at' => '2019-11-21 17:05:30',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'TRT',
                'slug' => 'trt',
                'desciription' => 'TRT',
                'file' => 'http://localhost/laboratuar/public/image/ZYvSZEpy1ahhputcucmnLf1IBLpSs4Zha1nLwZ9H.png',
                'created_at' => '2019-11-21 17:05:39',
                'updated_at' => '2019-11-21 17:05:39',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'MEDYA YÖNETİM',
                'slug' => 'medya-yonetim',
                'desciription' => 'MEDYA YÖNETİM',
                'file' => 'http://localhost/laboratuar/public/image/L4COxXi8Mnv921pNQ47DV9QmpyeeUamkk4EGnGcY.png',
                'created_at' => '2019-11-21 17:05:56',
                'updated_at' => '2019-11-21 17:05:56',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'SARAN HOLDİNG',
                'slug' => 'saran-holding',
                'desciription' => 'SARAN HOLDİNG',
                'file' => 'http://localhost/laboratuar/public/image/lsW5ZfqJJGp0UrrTmyrQHPZH0Wj9rAo377ssw8vp.png',
                'created_at' => '2019-11-21 17:06:15',
                'updated_at' => '2019-11-21 17:06:15',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'RADYO TRAFİK',
                'slug' => 'radyo-trafik',
                'desciription' => 'RADYO TRAFİK',
                'file' => 'http://localhost/laboratuar/public/image/rGfGs4y0IJYRCFPUTr2h9XrAnvJ6vc4vKooZtrBE.png',
                'created_at' => '2019-11-21 17:06:34',
                'updated_at' => '2019-11-21 17:06:34',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'RADYO SPOR',
                'slug' => 'radyo-spor',
                'desciription' => 'RADYO SPOR',
                'file' => 'http://localhost/laboratuar/public/image/ZIVhcd90csr9rdHcTO8nckwzPdvYpFtTRlg92w34.png',
                'created_at' => '2019-11-21 17:07:11',
                'updated_at' => '2019-11-21 17:07:11',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'UNITED MEDİA',
                'slug' => 'united-media',
                'desciription' => 'UNITED MEDİA',
                'file' => 'http://localhost/laboratuar/public/image/EwBCGaM3IhoSIIGYhhq4Y2M5iYPD5oqNdv0J3HPT.png',
                'created_at' => '2019-11-21 17:07:33',
                'updated_at' => '2019-11-21 17:07:33',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'name' => 'KENT FM',
                'slug' => 'kent-fm',
                'desciription' => 'KENT FM',
                'file' => 'http://localhost/laboratuar/public/image/sNJtdojF0hR7KqgQXGEq4lFOLos4dZjoGlKyBrph.png',
                'created_at' => '2019-11-21 17:07:47',
                'updated_at' => '2019-11-21 17:07:47',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'name' => '88.6 ISTANBUL FM',
                'slug' => '88-6-istanbul-fm',
                'desciription' => '88.6 ISTANBUL FM',
                'file' => 'http://localhost/laboratuar/public/image/ckp8VvWbsID6sNeMU4zRrd1Hp4XXZDivIEXLeVT2.png',
                'created_at' => '2019-11-21 17:08:40',
                'updated_at' => '2019-11-21 17:08:41',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'name' => 'PAL',
                'slug' => 'pal',
                'desciription' => 'PAL',
                'file' => 'http://localhost/laboratuar/public/image/afqdFxwM9i0zj6jVDaVG5Dk8PVTDNjktSfJpjOCj.png',
                'created_at' => '2019-11-21 17:09:05',
                'updated_at' => '2019-11-21 17:09:05',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'CLEAR LINE',
                'slug' => 'clear-line',
                'desciription' => 'CLEAR LINE',
                'file' => 'http://localhost/laboratuar/public/image/QiJCI6gnnfepOtGCjl7xBR2L9BJUgHYiHpGlfbpq.png',
                'created_at' => '2019-11-21 17:09:22',
                'updated_at' => '2019-11-21 17:09:22',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}