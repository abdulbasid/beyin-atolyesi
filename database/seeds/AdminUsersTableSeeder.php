<?php

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_users')->delete();
        
        \DB::table('admin_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'admin',
                'password' => '$2y$10$TITA7ShgX/d5Jqtlp47aTORJqgO0qZwsxL3xvIczDWj9GkBW/AE1y',
                'name' => 'Administrator',
                'avatar' => NULL,
                'remember_token' => 'o2aWGSt3ozqHFwWIfx8C6nTnzkGBeWYXb97uuret28WRHnRruyvFrsbrbeOb',
                'created_at' => '2020-01-16 20:25:10',
                'updated_at' => '2020-01-16 20:25:10',
            ),
        ));
        
        
    }
}