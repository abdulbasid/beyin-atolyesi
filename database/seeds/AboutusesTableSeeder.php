<?php

use Illuminate\Database\Seeder;

class AboutusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('aboutuses')->delete();
        
        \DB::table('aboutuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Beyin Atölyesi',
                'desciription' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deserunt fuga, harum quisquam quis nam fugit natus commodi voluptate totam at.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deserunt fuga, harum quisquam quis nam fugit natus commodi voluptate totam at.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deserunt fuga, harum quisquam quis nam fugit natus commodi voluptate totam at.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deserunt fuga, harum quisquam quis nam fugit natus commodi voluptate totam at.</p>',
                'file' => 'http://localhost/beyin-atolyesi/public/image/xlyiet73cJyF7nIwOeqIH0RcUewFUvQBEngooCSs.jpeg',
                'created_at' => '2019-12-03 13:59:21',
                'updated_at' => '2019-12-03 13:59:21',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}