<?php

use Illuminate\Database\Seeder;

class ReferancesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('referances')->delete();
        
        \DB::table('referances')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'bmw',
                'slug' => 'bmw',
                'desciription' => '<p>tesfet</p>',
                'file' => 'http://localhost/beyin-atolyesi/public/image/Dexr8plKRC3XSBJ5C4K35zNRgCnqf6bVNtak89Xh.jpeg',
                'category_id' => 1,
                'created_at' => '2019-12-02 19:00:04',
                'updated_at' => '2019-12-02 19:15:00',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'mercedess',
                'slug' => 'mercedess',
                'desciription' => '<p>mercedes</p>',
                'file' => 'http://localhost/beyin-atolyesi/public/image/VaMXMFNNJratJ31pL7xvrHQsJXRsdOSupjQBUoPS.jpeg',
                'category_id' => 1,
                'created_at' => '2019-12-02 19:00:24',
                'updated_at' => '2019-12-02 19:15:47',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'audii',
                'slug' => 'audii',
                'desciription' => '<p>audi</p>',
                'file' => 'http://localhost/beyin-atolyesi/public/image/SGBaDKJgcnz35V7RWviKTtndYmw5aE87EJEZXj3q.jpeg',
                'category_id' => 2,
                'created_at' => '2019-12-02 19:00:34',
                'updated_at' => '2019-12-02 19:16:34',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Volkswagenn',
                'slug' => 'volkswagenn',
                'desciription' => '<p>Volkswagen</p>',
                'file' => 'http://localhost/beyin-atolyesi/public/image/9mwZqzJQqHoltLdESBY0bNAO2XxsCDBsaIiifiOd.jpeg',
                'category_id' => 3,
                'created_at' => '2019-12-02 19:00:47',
                'updated_at' => '2019-12-02 19:17:32',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}