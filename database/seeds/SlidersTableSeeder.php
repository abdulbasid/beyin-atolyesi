<?php

use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sliders')->delete();
        
        \DB::table('sliders')->insert(array (
            0 => 
            array (
                'id' => 1,
                'type' => '0',
                'name' => 'Experienced',
                'desciription' => 'Our Team Is Experienced On More Than 4 Years For Guiding The Adventure. So Don\'t Be Afraid To Adventure With U',
                'slug' => 'experienced',
                'link' => NULL,
                'file' => 'http://localhost/dalis-okulu/public/image/0nw6zGuhyc41rEIEh8OceHRBLb98zTpdiz0AndcR.png',
                'created_at' => '2019-11-27 05:13:15',
                'updated_at' => '2019-11-27 05:13:15',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'type' => '0',
                'name' => 'Introducing',
                'desciription' => 'let\'s Make An Overseas Adventure, Visiting Island, Diving, Snorkling And More. And Trust Us As Your Travel Guide',
                'slug' => 'introducing',
                'link' => NULL,
                'file' => 'http://localhost/dalis-okulu/public/image/oZfnF5j72Dl9tCmo6F8XNUaxx96vit9zrHQtbVcP.png',
                'created_at' => '2019-11-27 05:13:54',
                'updated_at' => '2019-11-27 05:13:54',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}