<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(PostsTableSeeder::class);
         $this->call(ProjectsTableSeeder::class);
        // $this->call(SlidersTableSeeder::class);
        // $this->call(AboutusesTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(RefcategoriesTableSeeder::class);
        $this->call(ReferancesTableSeeder::class);
        $this->call(DivingtraningsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(TeamsTableSeeder::class);
        $this->call(ArticlesTableSeeder::class);
        
        $this->call(WhatwedoesTableSeeder::class);
        $this->call(HowdoesTableSeeder::class);
        $this->call(AdminMenuTableSeeder::class);
        $this->call(AdminOperationLogTableSeeder::class);
        $this->call(AdminPermissionsTableSeeder::class);
        $this->call(AdminRolesTableSeeder::class);
        $this->call(AdminRoleMenuTableSeeder::class);
        $this->call(AdminRolePermissionsTableSeeder::class);
        $this->call(AdminRoleUsersTableSeeder::class);
        $this->call(AdminUsersTableSeeder::class);
    }
}
