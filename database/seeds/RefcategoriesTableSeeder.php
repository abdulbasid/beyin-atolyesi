<?php

use Illuminate\Database\Seeder;

class RefcategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('refcategories')->delete();
        
        \DB::table('refcategories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'test',
                'slug' => 'test',
                'desciription' => '<p>reaf</p>',
                'created_at' => '2019-12-02 18:56:52',
                'updated_at' => '2019-12-02 18:56:52',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'test2',
                'slug' => 'test2',
                'desciription' => '<p>test2</p>',
                'created_at' => '2019-12-02 18:59:29',
                'updated_at' => '2019-12-02 18:59:29',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'test3',
                'slug' => 'test3',
                'desciription' => '<p>test3</p>',
                'created_at' => '2019-12-02 18:59:40',
                'updated_at' => '2019-12-02 18:59:40',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}