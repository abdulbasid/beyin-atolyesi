<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('events')->delete();
        
        \DB::table('events')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Demo Event-1',
                'start_date' => '2017-09-11',
                'end_date' => '2017-09-12',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Demo Event-2',
                'start_date' => '2017-09-11',
                'end_date' => '2017-09-13',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Demo Event-3',
                'start_date' => '2017-09-14',
                'end_date' => '2017-09-14',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Demo Event-4',
                'start_date' => '2017-09-17',
                'end_date' => '2017-09-17',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}