<?php

use Illuminate\Database\Seeder;

class AdminMenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_menu')->delete();
        
        \DB::table('admin_menu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 0,
                'order' => 13,
                'title' => 'Dashboard',
                'icon' => 'fa-bar-chart',
                'uri' => '/',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-01-16 20:42:56',
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 0,
                'order' => 14,
                'title' => 'Admin',
                'icon' => 'fa-tasks',
                'uri' => '',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-01-16 20:42:56',
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 2,
                'order' => 15,
                'title' => 'Users',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-01-16 20:42:56',
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 2,
                'order' => 16,
                'title' => 'Roles',
                'icon' => 'fa-user',
                'uri' => 'auth/roles',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-01-16 20:42:56',
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 2,
                'order' => 17,
                'title' => 'Permission',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-01-16 20:42:56',
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 2,
                'order' => 18,
                'title' => 'Menu',
                'icon' => 'fa-bars',
                'uri' => 'auth/menu',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-01-16 20:42:56',
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 2,
                'order' => 19,
                'title' => 'Operation log',
                'icon' => 'fa-history',
                'uri' => 'auth/logs',
                'permission' => NULL,
                'created_at' => NULL,
                'updated_at' => '2020-01-16 20:42:56',
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Slider',
                'icon' => 'fa-bars',
                'uri' => 'sliders',
                'permission' => '*',
                'created_at' => '2020-01-16 20:38:47',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 0,
                'order' => 2,
                'title' => 'Hakkımızda',
                'icon' => 'fa-bars',
                'uri' => NULL,
                'permission' => '*',
                'created_at' => '2020-01-16 20:38:55',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 9,
                'order' => 3,
                'title' => 'Hakkımızda',
                'icon' => 'fa-bars',
                'uri' => 'aboutuses',
                'permission' => '*',
                'created_at' => '2020-01-16 20:39:16',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 9,
                'order' => 4,
                'title' => 'Nasıl Yaparız?',
                'icon' => 'fa-bars',
                'uri' => 'howdoes',
                'permission' => '*',
                'created_at' => '2020-01-16 20:39:39',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 0,
                'order' => 5,
                'title' => 'Referanslar',
                'icon' => 'fa-bars',
                'uri' => NULL,
                'permission' => '*',
                'created_at' => '2020-01-16 20:39:57',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 12,
                'order' => 6,
                'title' => 'Kategoriler',
                'icon' => 'fa-bars',
                'uri' => 'refcategories',
                'permission' => '*',
                'created_at' => '2020-01-16 20:40:25',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 12,
                'order' => 7,
                'title' => 'Referanslar',
                'icon' => 'fa-bars',
                'uri' => 'referances',
                'permission' => '*',
                'created_at' => '2020-01-16 20:40:49',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            14 => 
            array (
                'id' => 15,
                'parent_id' => 0,
                'order' => 8,
                'title' => 'İş Ortakları',
                'icon' => 'fa-bars',
                'uri' => 'partners',
                'permission' => '*',
                'created_at' => '2020-01-16 20:41:33',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            15 => 
            array (
                'id' => 16,
                'parent_id' => 0,
                'order' => 9,
                'title' => 'Neler Yaparız?',
                'icon' => 'fa-bars',
                'uri' => 'whatwedoess',
                'permission' => '*',
                'created_at' => '2020-01-16 20:41:58',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            16 => 
            array (
                'id' => 17,
                'parent_id' => 0,
                'order' => 10,
                'title' => 'Ekibimiz',
                'icon' => 'fa-bars',
                'uri' => 'teams',
                'permission' => '*',
                'created_at' => '2020-01-16 20:42:10',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            17 => 
            array (
                'id' => 18,
                'parent_id' => 0,
                'order' => 11,
                'title' => 'Makaleler',
                'icon' => 'fa-bars',
                'uri' => 'articless',
                'permission' => '*',
                'created_at' => '2020-01-16 20:42:24',
                'updated_at' => '2020-01-16 20:42:56',
            ),
            18 => 
            array (
                'id' => 19,
                'parent_id' => 0,
                'order' => 12,
                'title' => 'iletişim',
                'icon' => 'fa-bars',
                'uri' => 'contacts',
                'permission' => '*',
                'created_at' => '2020-01-16 20:42:43',
                'updated_at' => '2020-01-16 20:42:56',
            ),
        ));
        
        
    }
}