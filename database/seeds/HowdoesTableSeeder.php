<?php

use Illuminate\Database\Seeder;

class HowdoesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('howdoes')->delete();
        
        \DB::table('howdoes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'slug' => 'tasarim',
                'title' => 'TASARIM',
                'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deserunt fuga, harum quisquam quis nam fugit natus commodi voluptate totam at.</p>',
                'file' => NULL,
                'created_at' => '2019-12-03 13:58:27',
                'updated_at' => '2019-12-03 13:58:27',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'slug' => 'gelistirme',
                'title' => 'GELİŞTİRME',
                'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deserunt fuga, harum quisquam quis nam fugit natus commodi voluptate totam at.</p>',
                'file' => NULL,
                'created_at' => '2019-12-03 13:58:34',
                'updated_at' => '2019-12-03 13:58:34',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'slug' => 'destek',
                'title' => 'DESTEK',
                'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi deserunt fuga, harum quisquam quis nam fugit natus commodi voluptate totam at.</p>',
                'file' => NULL,
                'created_at' => '2019-12-03 13:59:41',
                'updated_at' => '2019-12-03 13:59:41',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}