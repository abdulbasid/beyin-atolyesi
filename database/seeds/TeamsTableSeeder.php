<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teams')->delete();
        
        \DB::table('teams')->insert(array (
            0 => 
            array (
                'id' => 1,
                'desciription' => '<p>Haziran 1988 tarihinde İstanbul&rsquo;da doğdu. Hayatının 10 yıllık bir d&ouml;nemi İzmir sınırları i&ccedil;erisinde ge&ccedil;ti.Sakarya &Uuml;niversitesi, Makine M&uuml;hendisliği b&ouml;l&uuml;m&uuml;nden 2011 yılında mezun oldu. 2014 yılında Y&uuml;ksek Lisansını tamamladı.Şu an bir İtalyan firmanın T&uuml;rkiye ofisinde Teknik Servis M&uuml;d&uuml;rl&uuml;ğ&uuml; yapmakta. Ayrıca Aryasub Dalış Ekipmanları şirketinin y&ouml;neticisi 2015 Yılında Kemer&rsquo;in berrak ve bol balıklı sularında ilk dalışını yaptı. Tam tadını alamamış olacak ki, o g&uuml;nden bug&uuml;ne s&uuml;rekli olarak dalmakta.Dalış matematiğine ve dalış fiziğine y&uuml;ksek ilgi duymakta. Ayrıca ortamlarda &ldquo;bunun aynısını ben yaparım&rdquo; lafı ile bilinmekte. Şimdilik başarı oranı %50.</p>',
                'name' => 'TANZER KAY',
                'email' => NULL,
                'phone' => NULL,
                'job' => 'Teknik Servis Müdürlüğü',
                'unvan' => 'CMAS 3* DALICI / DALIŞ ASİSTANI',
                'city' => 'İstanbul',
                'instagram' => NULL,
                'twitter' => NULL,
                'facebook' => NULL,
                'slug' => 'tanzer-kay',
                'file' => 'http://localhost/beyin-atolyesi/public/image/J0nU5mei8f9aKtQWGsf12TZujF0p7h5agq8k8vPT.jpeg',
                'created_at' => '2019-12-01 21:19:24',
                'updated_at' => '2019-12-02 20:23:02',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}