<?php

use Illuminate\Database\Seeder;

class WhatwedoesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('whatwedoes')->delete();
        
        \DB::table('whatwedoes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'main_title' => 'Web Sitesi Tasarımı',
                'slug' => 'web-sitesi-tasarimi',
                'title' => 'Web Sitesi Tasarımı',
                'desciription' => '<p>Web&nbsp;Sitesi&nbsp;Tasarımı</p>',
                'file' => 'http://localhost/beyin-atolyesi/public/image/4ipfoBI5TdwwrjYdPQ9UC7LBLzIbAz7HmzapDU9E.jpeg',
                'created_at' => '2019-12-02 20:51:41',
                'updated_at' => '2019-12-02 21:04:47',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'main_title' => 'E-Ticaret Oluşturma ve Yönetimi',
                'slug' => 'e-ticaret-olusturma-ve-yonetimi',
                'title' => 'E-Ticaret Oluşturma ve Yönetimi',
                'desciription' => '<p>E-Ticaret&nbsp;Oluşturma&nbsp;ve&nbsp;Y&ouml;netimi</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:51:54',
                'updated_at' => '2019-12-02 20:51:54',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'main_title' => 'Yazılım Danışmanlığı / Proje Mentörlüğü',
                'slug' => 'yazilim-danismanligi-proje-mentorlugu',
                'title' => 'Yazılım Danışmanlığı / Proje Mentörlüğü',
                'desciription' => '<p>Yazılım&nbsp;Danışmanlığı&nbsp;/&nbsp;Proje&nbsp;Ment&ouml;rl&uuml;ğ&uuml;</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:52:05',
                'updated_at' => '2019-12-02 20:52:05',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'main_title' => 'Sosyal Medya Yönetim ve Raporlama',
                'slug' => 'sosyal-medya-yonetim-ve-raporlama',
                'title' => 'Sosyal Medya Yönetim ve Raporlama',
                'desciription' => '<p>Sosyal&nbsp;Medya&nbsp;Y&ouml;netim&nbsp;ve&nbsp;Raporlama</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:52:15',
                'updated_at' => '2019-12-02 20:52:15',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'main_title' => 'Kurumsal Kimlik ve Grafik Tasarım',
                'slug' => 'kurumsal-kimlik-ve-grafik-tasarim',
                'title' => 'Kurumsal Kimlik ve Grafik Tasarım',
                'desciription' => '<p>Kurumsal&nbsp;Kimlik&nbsp;ve&nbsp;Grafik&nbsp;Tasarım</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:52:24',
                'updated_at' => '2019-12-02 20:52:24',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'main_title' => 'Google, Facebook ve Instagram Reklamcılığı',
                'slug' => 'google-facebook-ve-instagram-reklamciligi',
                'title' => 'Google, Facebook ve Instagram Reklamcılığı',
                'desciription' => '<p>Google,&nbsp;Facebook&nbsp;ve&nbsp;Instagram&nbsp;Reklamcılığı</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:52:34',
                'updated_at' => '2019-12-02 20:52:34',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'main_title' => 'Ürün Fotoğrafçılığı ve Sanaltur Çekimi',
                'slug' => 'urun-fotografciligi-ve-sanaltur-cekimi',
                'title' => 'Ürün Fotoğrafçılığı ve Sanaltur Çekimi',
                'desciription' => '<p>&Uuml;r&uuml;n&nbsp;Fotoğraf&ccedil;ılığı&nbsp;ve&nbsp;Sanaltur&nbsp;&Ccedil;ekimi</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:52:44',
                'updated_at' => '2019-12-02 20:52:44',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'main_title' => 'Tanıtım Filmi, Masaüstü Reklam Filmi ve Animasyon',
                'slug' => 'tanitim-filmi-masaustu-reklam-filmi-ve-animasyon',
                'title' => 'Tanıtım Filmi, Masaüstü Reklam Filmi ve Animasyon',
                'desciription' => '<p>Tanıtım&nbsp;Filmi,&nbsp;Masa&uuml;st&uuml;&nbsp;Reklam&nbsp;Filmi&nbsp;ve&nbsp;Animasyon</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:52:52',
                'updated_at' => '2019-12-02 20:52:52',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'main_title' => 'Web, E-Ticaret sitesi Trafik ölçüm ve Raporlama',
                'slug' => 'web-e-ticaret-sitesi-trafik-olcum-ve-raporlama',
                'title' => 'Web, E-Ticaret sitesi Trafik ölçüm ve Raporlama',
                'desciription' => '<p>Web,&nbsp;E-Ticaret&nbsp;sitesi&nbsp;Trafik&nbsp;&ouml;l&ccedil;&uuml;m&nbsp;ve&nbsp;Raporlama</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:53:02',
                'updated_at' => '2019-12-02 20:53:02',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'main_title' => 'Sinema, Radyo ve Açıkhava Reklamcılğı',
                'slug' => 'sinema-radyo-ve-acikhava-reklamcilgi',
                'title' => 'Sinema, Radyo ve Açıkhava Reklamcılğı',
                'desciription' => '<p>Sinema,&nbsp;Radyo&nbsp;ve&nbsp;A&ccedil;ıkhava&nbsp;Reklamcılğı</p>',
                'file' => NULL,
                'created_at' => '2019-12-02 20:53:11',
                'updated_at' => '2019-12-02 20:53:11',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}