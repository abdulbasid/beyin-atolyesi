<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_tag', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('partner_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            
            $table->timestamps();
            $table->softDeletes();

            //relation
            $table->foreign('partner_id')->references('id')->on('partners')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tag_id')->references('id')->on('tags')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_tag');
    }
}
