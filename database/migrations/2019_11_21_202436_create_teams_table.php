<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('body', 5000)->nullable();
            $table->string('name', 128);
            $table->string('email', 128)->nullable();
            $table->string('phone', 128)->nullable();
            $table->string('job', 128);
            $table->string('unvan', 128);
            $table->string('city', 128);

            $table->string('instagram', 128)->nullable();
            $table->string('twitter', 128)->nullable();
            $table->string('facebook', 128)->nullable();

            $table->string('slug', 128);

            $table->string('file', 128)->nullable();

            $table->timestamps();
            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
