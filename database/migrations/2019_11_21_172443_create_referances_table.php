<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferancesTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referances', function (Blueprint $table) {
            $table->increments('id');
           
            $table->string('name', 128);
            $table->string('slug', 128)->unique();
            $table->string('desciription', 10000);
            $table->string('file', 128)->nullable();
            $table->integer('category_id')->unsigned();
            $table->enum('status', ['active', 'passive'])->default('active');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('category_id')->references('id')->on('refcategories')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referances');
    }
}
