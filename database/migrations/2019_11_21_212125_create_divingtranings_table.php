<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDivingTraningsTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divingtranings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('main_title', 128);
            $table->string('slug', 128);
            $table->string('title', 128);
            $table->string('desciription', 10000);

            $table->string('file', 128)->nullable();

            $table->timestamps();
            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divingtranings');
    }
}
