<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferanceTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referance_tag', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('referance_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            
            $table->timestamps();
            $table->softDeletes();

            //relation
            $table->foreign('referance_id')->references('id')->on('referances')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('tag_id')->references('id')->on('tags')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referance_tag');
    }
}
