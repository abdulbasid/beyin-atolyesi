@extends('layouts.login')

@section('content')
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{ asset('admin/media//bg/bg-3.jpg') }});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="{{ asset('web/images/medyabarter.png') }}">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                                <h3 class="kt-login__title">Şifremi Unuttum ?</h3>
                                <div class="kt-login__desc">Şifre Yenilemek için kayıtlı mail adresinizi giriniz:</div>
                            </div>
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <form class="kt-form"method="POST" action="{{ route('password.email') }}">
                                 {{ csrf_field() }}

                                    <input id="email" type="email" class="form-control" placeholder="Email Adreiniz" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif

                               

                                <div class="kt-login__actions">
                                    <button type="submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Gönder</button>&nbsp;&nbsp;
                                    <button onclick="window.location.href='{{ route('login') }}'" type="reset" class="btn btn-light btn-elevate kt-login__btn-secondary">Giriş Yap</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
