<!doctype html>
<html class="no-js" lang="zxx">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Beyin Atölyesi Dijital Medya Ajansı - Web sitesi, Yazılım Danışmanlığı, Sosyal Medya Yönetimi, Sinema Reklamı, Radyo Reklamı, Televizyon Reklamında doğru seçim.</title>
		<meta charset="UTF-8">
		<META HTTP-EQUIV='Expires' CONTENT='-1'>
		<META HTTP-EQUIV='Cache-Control' CONTENT='no-cache'>
		<META HTTP-EQUIV='Pragma' CONTENT='no-cache'>
		<META NAME='Title' CONTENT="Beyin Atölyesi Dijital Medya Ajansı - Web sitesi, Yazılım Danışmanlığı, Sosyal Medya Yönetimi, Sinema Reklamı, Radyo Reklamı, Televizyon Reklamında doğru seçim.">
		<META NAME='Description' CONTENT="Beyin Atölyesi sektör devleri diyebileceğimiz ajansların mutfaktaki yardımcısıdır. Beyin Atölyesi sadece web sayfaları hazırlamakla kalmaz, sosyal medyanızı yönetir, ihtiyacınız olan platformlar için aplikasyon yazar, logo tasarlar, katalog tasarlar, dilerseniz matbaa baskı sürecinizi yönetir. Sinema reklamları, radyo reklamları, televizyon reklamları ve bu reklamların çekimleri, hazırlanma sürecini üstlenir, Billboard ve outdoor reklamları hazırlar, Medya satınalma bütçenizi en uygun fiyatları alarak yönetir.">
		<META NAME='Keywords' LANG='tr' CONTENT="Burak YEKEZ, Web Sitesi Tasarımı, Tanıtım Filmi Çekim, Reklam Filmi Hazırlama, Sosyal Medya Yönetimi, Logo Tasarımı, Menü Tasarımı, Ürün Fotoğraf Çekim, Reklam Ajansı, Kurumsal Kimlik Tasarımı, Barter Reklam Ajansı, Barter, Sinema Reklamı, Sinema Reklamları, Sinemaya Reklam Vermek, Sinema Reklam Fiyatları, Radyo Reklamları, Televizyon Reklamları, Açıkhava Reklamları, Digital Reklam Ajansı, Sosyal Medya Reklamı, Radyo Jingle, Radyo Reklam Spotu, Billboard Reklamları, Cinemaximum Reklamları, Mars Media Reklam">
		<META NAME='Publisher' CONTENT='Burak YEKEZ - Beyin Atölyesi - http://www.beyinatolyesi.com'>
		<META NAME='Identifier-URL' CONTENT='http://www.google.com'>
		<META NAME='Identifier-URL' CONTENT='http://www.yandex.com'>
		<META NAME='Identifier-URL' CONTENT='http://www.bing.com'>
		<META NAME='Copyright' CONTENT="© 2003 - 2019 Beyin Atölyesi Dijital işler Ajansı by Burak Yekez">
		<META NAME='Reply-to' CONTENT='burakyekez@beyinatolyesi.com'>
		<META NAME='Robots' CONTENT='All'>
		<META NAME='Revisit-after' CONTENT='15'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        

    </head>
    
    @include('web.vendor.styles')


   
	
	
	
</head>
<body>

@include('web.header')



@yield('content')

@include('web.footer')
	
	
  
</body>
@include('web.vendor.scripts')
</html>