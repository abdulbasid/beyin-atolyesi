@extends('layouts.outside')

@section('content')
<section class="section banner-small parallax-section valign-wrap" style="background: url('assets/images/banner-small-2.jpg'); background-size: cover;">
    <div class="overlay"></div>
	<div class="content-wrap valign-bottom">
	<div class="container">
		<div class="col-md-12">
		<div class="title"><h1>Dalış Eğitimleri</h1></div>
		<div class="subtitle"><h2>{{ $divingtraning->title }}</h2></div>            
		</div>
	</div>
	</div><!--/.content-wrap -->
</section>

<div class="container">
<div class="iconic-title text-center col-md-12 no-h-padding sec-hq-pad-t sec-q-pad-b">
	<div class="col-md-12 no-h-padding">
		<div class="title-icon">
		<span><i class="pe-7s-anchor pe-2x"></i></span>
		</div>
	</div>
	<div class="col-md-12 no-h-padding">
		<h1>{{ $divingtraning->title }}</h1>
		<h2>Bali, Indonesia</h2>
	</div>
	</div>
	<div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2 margin-responsive sec-h-pad-b text-center">
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra vulputate tincidunt. Fusce ultricies dui pretium purus vestibulum suscipit. Proin ut turpis a mauris porttitor luctus eu quis velit. Nunc libero dolor, commodo sit amet nunc nec, sollicitudin semper ligula.</p>
	</div>
</div>


<section class="banner-carousel">
	<div class="container container-medium">
	<div class="col-md-12 no-h-padding">
		<div id="banner-carousel-alt" class="carousel slide wow animated fadeIn" data-ride="carousel">

		<div class="carousel-inner" role="listbox">
			<figure class="item active">
			<img src="{{ asset('web/images/banner-small-4.jpg') }}" alt="Portfolio image">

			
			</figure>
		</div>

	
		<!-- <a class="left carousel-control" href="#banner-carousel-alt" role="button" data-slide="prev">
			<div class="control-button def-btn btn-solid left"><i class="fa fa-angle-left"></i></div>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#banner-carousel-alt" role="button" data-slide="next">
			<div class="control-button def-btn btn-solid right"><i class="fa fa-angle-right"></i></div>
			<span class="sr-only">Next</span>
		</a> -->
		</div>
	</div>
	</div>
</section>


<section class="portfolio-single container-fluid no-h-padding" id="portfolio">
	<div class="container-medium clearfix sec-hq-pad">
	<div class="row">
		<div class="col-md-6 description margin-responsive">
		<h3 class="textbold wow animated fadeIn mar-b-20">Açıklama</h3>
		<p class=" wow animated fadeIn mar-b-20">

		{{ $divingtraning->body }}

		</div>
		<div class="col-md-6 client-testimonial">
		<div class="client-bio wow animated fadeIn">
			<div class="client-photo"><img src="{{ asset('web/images/client.jpg') }}" alt="Clients Photo"></div>
			<div class="name-position">
			<h4 class="name">Veronica Lee</h4>
			<p class="position">Co-Founder</p>
			</div>
			<div class="client-brand">
			<img src="{{ asset('web/images/client-logo-single.png') }}" alt="Clients Brand">
			</div>
		</div>
		<div class="client-comment wow animated fadeIn">
			<h4 class="main-comment">Great Services, Very satisfied</h4>
			<p class="text-comment mar-t-20">
			<span class="quote-bg"><i class="fa fa-quote-right"></i></span>
			Praesent faucibus nisi eu ipsum sollicitudin gravida. Proin nec aliquet eros. Phasellus vel tempor enim, non finibus leo. Nam finibus lobortis lectus non mattis. Integer a orci nec ante pharetra rhoncus. Maecenas quam est, laoreet sit amet consequat non, porta eget erat. Etiam vel metus non tortor congue pellentesque at id augue.</p>
		</div>
		</div>
	</div>
	</div>

</section>


@include('web.home.logos')


@endsection