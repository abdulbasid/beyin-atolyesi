<!-- Feature Panel Begin -->
<section class="feature-panel-wrap sec-pad">
    <div class="container">
    <div class="feature-panel panel with-nav-tabs">
        <div class="panel-body">
        <div class="tab-content">

            <div class="tab-pane fade in active" id="tabfeature1">
            <div class="col-md-5 no-h-padding">
                <div class="image">
                <img src="{{ asset('web/images/feature-panel1.jpg' ) }}" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="description">
                <div class="title">
                    <h4 class="underlined-heading">Trusted Water Adventure Agent</h4>
                </div>
                <div class="content">
                    <p>We are trusted by more than 3000 adventurer on the world, and we have travelled about a hundred places, with this statistic so why you still in your home sitting and do nothing, Let's go to feel the air of the wild nature. Just contact us to see our travel schedule. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed finibus nibh. Vestibulum pharetra blandit mollis ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div><!--/.content -->

                <a href="#" class="def-btn btn-outline more-info">More Info</a>
                </div><!--/.description -->
            </div>
            </div><!--/.tab-pane -->

            <div class="tab-pane fade in" id="tabfeature2">
            <div class="col-md-5 no-h-padding">
                <div class="image">
                <img src="{{ asset('web/images/feature-panel2.jpg' ) }}" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="description">
                <div class="title">
                    <h4 class="underlined-heading">We Have Best Adventure Gear</h4>
                </div>
                <div class="content">
                    <p>Our agency was sponsored by some of the best adventure gear product on the world, so you will be very safe when using our gear while you make your adventure. And we also have more than hundred of adventure gears so we are very ready for make your adventure plan come true. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed finibus nibh. Vestibulum pharetra blandit mollis ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div><!--/.content -->

                <a href="#" class="def-btn btn-outline more-info">More Info</a>
                </div><!--/.description -->
            </div>
            </div><!--/.tab-pane -->

            <div class="tab-pane fade in" id="tabfeature3">
            <div class="col-md-5 no-h-padding">
                <div class="image">
                <img src="{{ asset('web/images/feature-panel3.jpg' ) }}" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="description">
                <div class="title">
                    <h4 class="underlined-heading">Professional Guide</h4>
                </div>
                <div class="content">
                    <p>With average more than 4 years experience on guiding adventurer and many places has been travelled by our team, it makes our team is very capable to guide you whiile doing your adventure and make you always safe, enjoy and feel very protected. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sed finibus nibh. Vestibulum pharetra blandit mollis ipsum dolor sit amet, consectetur adipiscing elit ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div><!--/.content -->

                <a href="#" class="def-btn btn-outline more-info">More Info</a>
                </div><!--/.description -->
            </div>
            </div><!--/.tab-pane -->

        </div><!--/.tab-content -->

        </div><!--/.panel-body -->

        <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li class="active">
            <a href="#tabfeature1" data-toggle="tab" class="no-effect">
                <i class="pe-7s-like2"></i>
            </a>
            </li>
            <li>
            <a href="#tabfeature2" data-toggle="tab" class="no-effect">
                <i class="pe-7s-config"></i>
            </a>
            </li>
            <li>
            <a href="#tabfeature3" data-toggle="tab" class="no-effect">
                <i class="pe-7s-medal"></i>
            </a>
            </li>
        </ul>
        </div><!--/.panel-heading -->
    </div><!--/.feature-panel -->
    </div><!--/.container -->
</section><!--/.feature-panel-wrap -->
<!-- Feature Panel End -->

 <!-- Who We Are Begin -->
 <section class="who-we-are">
    <div class="image-bg" style="background: url('{{ asset('web/images/more-about-us.jpg')}}') center no-repeat; background-size: cover;"></div>
    <div class="container">
        <div class="valign-wrap">
            <div class="col-md-6 col-sm-6 valign-middle image-content no-h-padding">
            <div class="image-title">Our Team Photograph</div>
            </div><!--/.image-content -->
            <div class="col-md-6 col-sm-6 valign-middle text-content">
            <div class="title">
                <h3 class="underlined-heading wow animated fadeInUp">More About Us</h3>
            </div>
            <div class="text wow animated fadeIn"><p>We have a best team in the world that have skilled on some adventure thing below.</p></div>
            <div class="icon-list-wrap">
                <div class="row">
                <div class="icon-list col-md-6 col-sm-6">
                    <div class="icon text-center"><i class="pe-7s-anchor"></i></div>
                    <div class="icon-text">
                    <div class="top-text">Our Team Is</div>
                    <div class="bottom-text">Best Divers</div>
                    </div>
                </div>
                <div class="icon-list col-md-6 col-sm-6">
                    <div class="icon text-center"><i class="pe-7s-compass"></i></div>
                    <div class="icon-text">
                    <div class="top-text">Our Team Is</div>
                    <div class="bottom-text">Best Hikers</div>
                    </div>
                </div>
                <div class="icon-list col-md-6 col-sm-6">
                    <div class="icon text-center"><i class="pe-7s-plane"></i></div>
                    <div class="icon-text">
                    <div class="top-text">We Are</div>
                    <div class="bottom-text">Great Traveler</div>
                    </div>
                </div>
                <div class="icon-list col-md-6 col-sm-6">
                    <div class="icon text-center"><i class="pe-7s-way"></i></div>
                    <div class="icon-text">
                    <div class="top-text">Our Team Is</div>
                    <div class="bottom-text">Best Explorer</div>
                    </div>
                </div>
                </div>
            </div>
            </div><!--/.text-content -->
        </div>
    </div>
</section><!--/.who-we-are -->
<!-- Who We Are End -->