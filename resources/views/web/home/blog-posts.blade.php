 <!-- Blog Post Preview Begin -->
 <section class="blog-post-preview">
    <div class="image-bg" style="background: url('{{ asset('web/images/blog-post-preview.jpg') }}') center no-repeat; background-size: cover;"></div>
    <div class="container">
    <div class="valign-wrap">
        <div class="col-md-6 col-sm-6 valign-middle image-content no-h-padding">
        <div class="image-title">Our Office Environment</div>
        </div><!--/.image-content -->
        <div class="col-md-6 col-sm6 valign-middle text-content">
        <div class="top-content">
            <div class="date">
            Oct 15, 2015
            </div>
            <div class="icon-text-wrap">
            <div class="icon-text">
                <i class="pe-7s-like"></i>
                2 Likes
            </div>
            <div class="icon-text">
                <i class="pe-7s-share"></i>
                2 Likes
            </div>
            <div class="icon-text">
                <i class="pe-7s-comment"></i>
                2 Likes
            </div>
            </div>
        </div>
        <div class="text wow animated fadeIn"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis at magna et hendrerit. Aenean consectetur velit vitae ipsum tristique rutrum. Proin in scelerisque tellus, tincidunt posuere enim. Aliquam vitae dictum eros. Cras fermentum diam justo, sed lobortis mauris iaculis a</p></div>
        
        <div class="button-wrap col-md-12 no-h-padding">
            <a href="#" class="def-btn btn-outline">Read More</a>
        </div>
        </div><!--/.text-content -->
    </div>
    </div>
</section><!--/.blog-post-preview -->
<!-- Blog Post Preview End -->