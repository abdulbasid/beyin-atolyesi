<!-- Blog Post Begin -->


<div class="blog-area ptb-90 b-style-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="section-title">
                    <h2>Yazılan Son Blog Yazılar</h2>
                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. Pellentesque mollis eros vel mattis tempor.</p> -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="blog-active">
            @foreach($lastarticles as $article)
                <div class="col-md-4 col-sm-4">
                    <div class="blog-text">
                        <div class="blog-img">
                        @if($article->file)
                            <img alt="{{ $article->name }}" src="{{ $article->file }}"  class="fullwidth"  style="height: 250px!important;"/>
                        @endif
                            <div class="blog-view">
                                <a href="{{ route('article', $article->slug) }}">
                                    <i class="fa fa-link" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <div class="blog-info white-bg">
                            <div class="blog-meta">
                                <div class="blog-meta-left">
                                    <span>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        {{ $article->created_at }}
                                    </span>
                                    <span class="blog-middel">
                                        <i class="fa fa-user"></i>
                                        {{ $article->team->name }}
                                    </span>
                                </div>
                                <div class="blog-meta-left meta-right">
                                    <span>
                                            <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                            <a href="#">0 comment</a>
                                        </span>
                                </div>
                            </div>
                            <div class="blog-info-top">
                                <h3><a href="{{ route('article', $article->slug) }}">{{ $article->name }}</a></h3>
                                <p>{{ substr($article->body,0,250)  }}...</p>
                                <a href="{{ route('article', $article->slug) }}">
                                    Devamı
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
             @endforeach
            </div>
        </div>
    </div>
</div>
