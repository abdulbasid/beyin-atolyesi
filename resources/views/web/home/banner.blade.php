<div class="slider-area-4">
    <div class="single-slider-img">
        
        <div class="single-slider-4 youtube-bg" style="background-image: url({{ asset('web/img/slider/11.jpg') }});" data-property="{videoURL:'tWI_j-9x_EI',mute:true}">
            <div class="slider-title-4">
            @foreach($sliders as $slider)
                <h2> {{ $slider->name }}</h2>
                <p>Profesyönel olduklarını söyleyen ajanslara projelerinizi verdiniz ve dijital işleriniz hala istediğiniz gibi değil. <br> O insanlara teşekkür edin. Onlar sayesinde buradasınız.</p>
            @endforeach
            </div>
        </div>                  
    </div>
</div>	