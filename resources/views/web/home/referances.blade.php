<div class="portfolio-area ptb-90">
    <div class="container">
        <div class="portfolio-menu text-center portfolio-left-menu mb-50">
            
            <button class="active" data-filter="*">TÜMÜ</button>
            @foreach($refcategories as $refcategory)
            <button data-filter=".{{ $refcategory->slug }}">{{ $refcategory->name }} </button>
            @endforeach
            
        </div>			
            <div class="grid">
                <div class="row portfolio-style-2">
                
                @foreach($homearticles as $ref)
                <div class="col-md-6 col-sm-6 col-xs-12 grid-item {{ $ref->category->slug }} mb-30">
                    <div class="portfolio hover-style9">
                        <div class="portfolio-img">
                        @if($ref->file)
                            <img alt="{{ $ref->name }}" src="{{ $ref->file }}"  class="fullwidth"  style="height: 250px!important;"/>
                        @endif
                            <div class="portfolio-view">
                                <a class="img-poppu" href="img/neyaptik/web/laboratuar.jpg">
                                    <i class="icon-focus"></i>
                                </a>								
                            </div>
                        </div>
                        <div class="portfolio-title-2 text-center title-color-2">
                            <h3><a href="{{ route('referance', $ref->slug) }}">{{ $ref->name }}</a></h3>
                            <span>{{ substr($ref->body,0,150)  }}...</span>
                        </div>									
                    </div>
                </div>
                 @endforeach
   
                
              
            </div>		
        </div>
        <div class="view-more text-center">
            <a target="blank" class="button-hover" href="{{ route('web.pages.referances') }}">Daha Fazlası</a>
        </div>
    </div>
</div>		