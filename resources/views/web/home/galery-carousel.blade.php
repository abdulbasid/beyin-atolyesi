<!-- Gallery Carousel Wrap Begin -->
<section class="gallery-carousel-wrap sec-pad">
    <div class="container">
    <div class="title">
        <h3 class="underlined-heading wow animated fadeInUp">Eklenen Son Fotoğraflar</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra vulputate tincidunt. Fusce ultricies dui pretium purus vestibulum suscipit. Proin ut turpis a mauris porttitor luctus eu quis velit. Nunc libero dolor, commodo sit amet nunc nec, sollicitudin semper ligula.</p>
    </div>

    <div class="gallery-carousel wow animated fadeIn">
        <div class="image">
        <img src="{{ asset('web/images/gallery-carousel1.jpg') }}" alt="">
        </div>
        <div class="image">
        <img src="{{ asset('web/images/gallery-carousel2.jpg') }}" alt="">
        </div>
        <div class="image">
        <img src="{{ asset('web/images/gallery-carousel3.jpg') }}" alt="">
        </div>
        <div class="image">
        <img src="{{ asset('web/images/gallery-carousel4.jpg') }}" alt="">
        </div>
        <div class="image">
        <img src="{{ asset('web/images/gallery-carousel5.jpg') }}" alt="">
        </div>
    </div><!--/.client-logos -->
    </div><!--/.container-fluid -->
</section><!--/.gallery-carousel-wrap -->
<!-- Gallery Carousel Wrap End -->