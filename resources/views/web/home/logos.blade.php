
<section class="client-logos-wrap">
    <div class="container">
        <div class="client-logos wow animated fadeInUp">
            <div class="client-logo">
            @foreach($referances as $referance)
                @if($referance->file)
                <img src="{{ $referance->file }}" alt="{{ $referance->title }}">
                @endif
            </div>
            @endforeach
        </div>
    </div>
</section>
