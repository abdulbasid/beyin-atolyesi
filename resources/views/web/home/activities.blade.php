    
    <section class="title-paragraph sec-pad-t">
      <div class="container">
        <div class="col-md-9 col-sm-6 margin-responsive">
          <h3 class="underlined-heading wow animated fadeInUp">Etkinliklerimiz</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur viverra vulputate tincidunt. Fusce ultricies dui pretium purus vestibulum suscipit. Proin ut turpis a mauris porttitor luctus eu quis velit. Nunc libero dolor, commodo sit amet nunc nec, sollicitudin semper ligula.</p>
        </div>
      </div>
    </section>
   


    <section class="portfolio-collase sec-pad no-b-padding">
      <div class="container-fluid no-h-padding">
      @foreach($homeposts as $post)
        <div class="col-md-3 col-sm-6 no-h-padding">
          <div class="portfolio-item">
            <figure class="portfolio-figure">
                @if($post->file)
                    <img src="{{ $post->file }}" class="img-responsive">
                @endif
              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <h2>{{ $post->name }}</h2>
                    <div class="separator"></div>
                    <p>Türkiye, {{ $post->city }}</p>
                    <a href="{{ route('post', $post->slug) }}" class="def-btn btn-outline portfolio-btn">
                      <span class="text-content">
                      Etkinlik Detayı <i class="pe-7s-angle-right-circle"></i></span>
                    </a>
                  </div>
                </div>
              </figcaption>
            </figure>
          </div>
        </div>

        @endforeach

       
      </div>
    </section>
    