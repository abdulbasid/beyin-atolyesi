<div class="team-area pt-90 pb-60">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        <h2>Harika Ekibimiz</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum
                            massa. Pellentesque mollis eros vel mattis tempor.</p>
                    </div>
                </div>
            </div>
            <div class="row">
            @foreach($teams as $team)
                <div class="col-md-3 col-sm-6">
                    <div class="meet-all-four mb-30">
                        <div class="meet-img-four">
                            <a href="#">
                                 @if($team->file)
                                    <img alt="{{ $team->name }}" src="{{ $team->file }}"  class="fullwidth"  style="height: 250px!important;"/>
                                @endif
                            </a>
                            <div class="meet-icon-all-four">
                                <div class="meet-icon-four">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-link" aria-hidden="true"></i></a> </li>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="product-content-four text-center">
                            <h3>{{ $team->name }}</h3>
                            <p>{{ $team->job }}, {{ $team->unvan }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
