<div class="about-counter-area ptb-90 parallax-bg" data-stellar-background-ratio="0.6">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6 text-center">
                <div class="single-counter">
                    <h3 class="about-counter">150</h3>
                    <p>Toplam Proje</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 text-center">
                <div class="single-counter">
                    <h3 class="about-counter">100</h3>
                    <p>İş Ortakları</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 text-center">
                <div class="single-counter">
                    <h3 class="about-counter">120</h3>
                    <p>Tamamlanmış Proje</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6 text-center">
                <div class="single-counter">
                    <h3 class="about-counter">170</h3>
                    <p>Bardak Kahve</p>
                </div>
            </div>
        </div>
    </div>
</div>