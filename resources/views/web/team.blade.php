@extends('layouts.outside')

@section('content')

   <section class="section banner-small parallax-section valign-wrap" style="background: url('{{ asset('web/images/banner-small-2.jpg') }}') }}'); background-size: cover;">
      <div class="overlay"></div>
      <div class="content-wrap valign-bottom">
        <div class="container">
        <div class="col-md-1 no-h-padding">
            <div class="image">
              <div class="shape">
                <img src="http://localhost/dalis-okulu/public/web/images/person4.jpg" alt="">
              </div>
            </div>
          </div>
          <div class="col-md-11">
            
            <div class="title"><h1>{{ $team->name }} Hakkında Detay</h1></div>
            <div class="subtitle"><h2>{{ $team->job }}, {{ $team->unvan }}</h2></div>            
          </div>
        </div>
      </div>
    </section>

    <div class="container">
      <div class="col-md-8 sec-pad-t">
        <article class="blog-post col-md-12 no-h-padding wow animated fadeIn">
          <div class="image-content col-md-12 no-h-padding">
            <div class="image">
              <img src="{{ asset('web/images/blog-list-img-3.jpg') }}" alt="">
            </div>
            <div class="col-lg-5 col-md-4 col-sm-4 date">Oct 12, 2016</div>
          
          </div>

          <div class="text-content col-md-12 no-h-padding margin-responsive">
            <div class="title wow animated fadeInUp">
              <a href="#"><h1>{{ $team->name }}</h1></a>
            </div>
            <div class="cat-name text-left">
              <a href="" class="link">Türkiye</a> /
              <a href="" class="link">{{ $team->city }}</a>
            </div>
            <div class="text wow animated fadeIn">
              <p>
              {{ $team->body }}
              </p>
            </div>

            

          </div>
        </article>

      </div>
      
      <div class="col-md-4 blog-sidebar sec-pad-t">
        

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Katıldığı Etkinlikler</h3>
          </div>

        
        
          <div class="content">
          @foreach($postteams as $postteam)
           @if($team->id == $postteam->team_id)
              <div class="media blog-sidebar-list">
                <div class="media-left">
                  <a href="#">
                    <img class="media-object" src="{{ asset('web/images/blog-sidebar-img-1.jpg') }}" alt="...">
                  </a>
                </div>
                <div class="media-body">
                  <h4 class="media-heading"><a href="blog-single.html">{{ $postteam->post->name }}</a></h4>
                  <p>Türkiye / {{$postteam->post->city}}</p>
                </div>
              </div>
          </div>
          @endif
         @endforeach
        </div>
        
      

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Tags</h3>
          </div>

          <div class="content">
            <div class="blog-sidebar-tags">
            @foreach($posttags as $posttag)
              @if($team->id == $posttag->tag_id)
                <a href="#" class="tag def-btn btn-outline">{{ $posttag->post->name }}</a>
              @endif
            @endforeach 
            </div>
          </div>
        </div>
      </div>
    </div>
        
@endsection