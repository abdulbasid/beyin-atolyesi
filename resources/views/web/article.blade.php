@extends('layouts.outside')

@section('content')
<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="breadcrumbs breadcrumbs-3">
                            <h2 class="page-title">{{ $article->name }}</h2>
                            <ul>
                                <li>
                                    <a href="{{route('welcome')}}">Anasayfa</a>
                                </li>
                                <li>
                                    <a class="active" >Blog</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!-- breadcrumbs end -->
        <section class="blog-details-area ptb-90">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                        <div class="blog-all-details">
                            <div class="blog-text-2 mb-30">
                                <div class="blog-img">
                                @if($article->file)
                                    <img alt="{{ $article->name }}" src="{{ $article->file }}"  class="fullwidth" style=";object-fit:cover"/>
                                @endif
                                </div>
                                <div class="blog-info white-bg">
                                    <div class="blog-meta">
                                        <div class="blog-meta-left">
                                            <span>
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ $article->created_at }}
                                            </span>
                                            
                                        </div>
                                        <div class="blog-meta-left meta-right">
                                            <span>
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                <a href="#">{{ $article->team->name }}</a>
                                            </span>
                                        </div>
                                        <div class="blog-meta-left meta-right">
                                            <span>
                                                <i class="fa fa-commenting-o" aria-hidden="true"></i>
                                                <a href="#">6 comment</a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="blog-info-top">
                                        <h3>{{ $article->name }}</h3>
                                        {{ $article->body }}
                                    </div>
                                </div>
                            </div>
                            <div class="news-details-bottom mtb-60">
                                <h3 class="leave-comment-text">Yorumlar</h3>
                                <div class="blog-top">
                                    <div class="news-allreply">
                                        <a href="#"><img src="img/blog/5.jpg" alt=""></a>
                                    </div>
                                    <div class="blog-img-details">
                                        <div class="blog-title">
                                            <div class="blog-title-1">
                                                <h3>jane smith</h3>
                                                <span>14 february, 2018 at 7 : 00 pm</span>
                                            </div>
                                            <div class="nes-icon">
                                                <a href="#">
                                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <p class="p-border">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore i aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                    </div>
                                </div>
                                <div class="blog-top mt-40 mb-40 margin-middle">
                                    <div class="news-allreply">
                                        <a href="#"><img src="img/blog/6.jpg" alt=""></a>
                                    </div>
                                    <div class="blog-img-details">
                                        <div class="blog-title">
                                            <div class="blog-title-1">
                                                <h3>jane smith</h3>
                                                <span>14 february, 2018 at 7 : 00 pm</span>
                                            </div>
                                            <div class="nes-icon">
                                                <a href="#">
                                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <p class="p-border">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore i aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                    </div>
                                </div>
                                <div class="blog-top">
                                    <div class="news-allreply">
                                        <a href="#"><img src="img/blog/7.jpg" alt=""></a>
                                    </div>
                                    <div class="blog-img-details">
                                        <div class="blog-title">
                                            <div class="blog-title-1">
                                                <h3>jane smith</h3>
                                                <span>14 february, 2018 at 7 : 00 pm</span>
                                            </div>
                                            <div class="nes-icon">
                                                <a href="#">
                                                    <i class="fa fa-reply" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <p class="p-border">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore i aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo cons. Duis aute irure dolor in reprehenderit in </p>
                                    </div>
                                </div>
                            </div>
                            <div class="leave-comment">
                                <h3 class="leave-comment-text">Yorum Yaz</h3>
                                <form action="#">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="leave-form">
                                                <input placeholder="Ad*" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="leave-form">
                                                <input placeholder="Email*" type="text">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text-leave">
                                                <textarea placeholder="Yorum*"></textarea>
                                                <button class="submit" type="submit">Gönder</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="blog-right-sidebar">
                            <div class="blog-search mb-60">
                                <h3 class="leave-comment-text">Ara</h3>
                                <form action="#">
                                    <input value="" placeholder="Ara" type="text">
                                    <button class="submit" type="submit"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                </form>
                            </div>
                            <div class="blog-right-sidebar-top mb-60">
                                <h3 class="leave-comment-text">Eski Yazılar</h3>
                                <div class="recent-all">
                                @foreach($lastarticles as $lastarticle)
                                    <div class="recent-img-text mb-20">
                                        <div class="recent-img">
                                            <a href="{{ route('article', $article->slug) }}"><img src="{{ asset('web/img/blog/5.jpg') }}" alt=""></a>
                                        </div>
                                        <div class="recent-text">
                                            <h4>
                                                <a href="{{ route('article', $lastarticle->slug) }}">{{ $lastarticle->name }}</a>
                                            </h4>
                                            <p>{{ $lastarticle->team->name }}</p>
                                        </div>
                                    </div>
                                @endforeach
                                </div>
                            </div>
                            
                            <div class="blog-right-sidebar-bottom">
                                <h3 class="leave-comment-text">Tags</h3>
                                <ul>
                                @foreach($articletags as $articletag)
                                    @if($article->id == $articletag->article_id)
                                    <li><a href="#">#{{ $articletag->tag->name }}</a></li>
                                    @endif
                                @endforeach 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


@endsection