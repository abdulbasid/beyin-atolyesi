@extends('layouts.outside')

@section('content')

<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs breadcrumbs-3">
                    <h2 class="page-title">{{ $whatwedo->title }}</h2>
                    <ul>
                        <li><a href="{{ route('welcome')}}">Anasayfa</a></li>
                        <li>Neler Yaparız?</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumbs end -->
<section class="portfolio-details ptb-90">
    <div class="container">
    <div class="row">
            <div class="col-md-12">
                
                @if($whatwedo->file)
                    <img alt="{{ $whatwedo->name }}" src="{{ $whatwedo->file }}"  class="fullwidth" width="100%"/>
                @endif
            
            </div>
        </div>
        <div class="row pt-60">
            <div class="col-md-8">
                <div class="project-desc">
                    <h3 class="desc">Açıklama</h3><br>
                    {{ $whatwedo->body }}
                </div>
    
                <div class="post-share">
                    <ul>
                        <li>Paylaş : </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>						
            </div>
            <div class="col-md-4">
                <div class="portfolio-meta">
                    <h3 class="desc">Makale Bilgileri</h3>
                    <ul>
                        <li><i class="fa fa-user"></i><span>Yazar :</span> </li>
                        <li><i class="fa fa-calendar"></i><span>Paylaşım Tarihi :</span></li>
                        
                        <li><i class="fa fa-link"></i><span>Kategori : </span><a href="#">test</a></li>
                         <li><i class="fa fa-coffee"></i><span>Etiketler :</span>HTML5 / WP / CSS3 / PHP</li>
                    </ul>
                </div>					
            </div>
        </div>
    </div>
</section>

<!-- <div class="subscribe-area gray-bg ptb-90">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 text-center">
                <div class="newsletter">
                    <p>you can follow me by email</p>
                    <h2>subscribe</h2>
                    <form class="pstn" action="#">
                        <input placeholder="E-mail" type="email">
                        <button type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->


@endsection