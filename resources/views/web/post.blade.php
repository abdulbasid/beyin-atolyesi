@extends('layouts.outside')

@section('content')
   <!-- Banner Small Begin -->
   <section class="section banner-small parallax-section valign-wrap" style="background: url('{{ asset ('web/images/banner-small-2.jpg') }}'); background-size: cover;">
      <div class="overlay"></div>
      <div class="content-wrap valign-bottom">
        <div class="container">
          <div class="col-md-12">
            <div class="title"><h1>{{ $post->name }}</h1></div>
            <div class="subtitle"><h2>Türkiye / {{ $post->city }}</h2></div>            
          </div>
        </div>
      </div><!--/.content-wrap -->
    </section><!--/.banner-small -->
    <!-- Banner Small End -->

    <!-- Blog Post Begin -->
    <div class="container">
      <div class="col-md-8 sec-pad-t">
        <article class="blog-post col-md-12 no-h-padding wow animated fadeIn">
          <div class="image-content col-md-12 no-h-padding">
            <div class="image">
              <img src="{{ asset ('web/images/blog-list-img-3.jpg') }}" alt="">
            </div>
            <div class="col-lg-5 col-md-4 col-sm-4 date">Oct 12, 2016</div>
            <div class="col-lg-7 col-md-8 col-sm-8 info text-right">
           
            </div>
          </div><!--/.image-content -->

          <div class="text-content col-md-12 no-h-padding margin-responsive">
            <div class="title wow animated fadeInUp">
              <a href="#"><h1>{{ $post->name }}</h1></a>
            </div>
            <div class="cat-name text-left">
              <a href="" class="link">Türkiye</a> /
              <a href="" class="link">{{ $post->city }}</a>
            </div>
            <div class="text wow animated fadeIn">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.
              </p>
            </div>

          
            <div class="title mar-b-20 wow animated fadeIn">
              <h2>Etkinlik Fotoğrafları</h2>
            </div>
            <div class="image wow animated fadeIn">
              <img src="{{ asset ('web/images/blog-list-img-4.jpg') }}" alt="" class="fullwidth">
            </div>
            <div class="text wow animated fadeIn">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.
              </p>
            </div>
          </div><!--/.text-content -->
        </article><!--/.blog-post -->

        <div class="article-tags col-md-12 no-h-padding sec-h-pad-b wow animated fadeIn">
          <h4 class="tag-title">Etiketler </h4>
          @foreach($posttags as $posttag)
            @if($post->id == $posttag->post_id)
              <a href="#" class="tag def-btn btn-outline">{{ $posttag->tag->name }}</a>
            @endif
          @endforeach 
        </div>

        <div class="article-share col-md-12 no-h-padding sec-h-pad-b wow animated fadeIn">
          <h4 class="share-title">Paylaş </h4>
          <a href=""><i class="fa fa-facebook"></i></a>
          <a href=""><i class="fa fa-twitter"></i></a>
          <a href=""><i class="fa fa-google-plus"></i></a>
          <a href=""><i class="fa fa-instagram"></i></a>
        </div>



      </div>
      
      <div class="col-md-4 blog-sidebar sec-pad-t">
      
        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Son Etkinlikler</h3>
          </div>

          <div class="content">

          @foreach($lastposts as $lastpost)
            <div class="media blog-sidebar-list">
              <div class="media-left">
                <a href="{{ route('post', $post->slug) }}">
                  <img class="media-object" src="{{ asset ('web/images/blog-sidebar-img-1.jpg') }}" alt="...">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading"><a href="{{ route('post', $post->slug) }}">{{ $lastpost->name }}</a></h4>
                <p>Türkiye / {{ $lastpost->city }}</p>
              </div>
            </div><!--/.blog-sidebar-list -->
          @endforeach


          </div><!--/.content -->
        </div><!--/.content-row -->

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Arşiv</h3>
          </div>

          <div class="content">
            <ul class="blog-sidebar-list list-unstyled">
              <li><a href="#">January (4)</a></li>
              <li><a href="#">February (4)</a></li>
              <li><a href="#">March (4)</a></li>
              <li><a href="#">April (4)</a></li>
              <li><a href="#">May (4)</a></li>
            </ul><!--/.blog-sidebar-list -->
          </div><!--/.content -->
        </div><!--/.content-row -->

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Etiketler</h3>
          </div>

          <div class="content">
            <div class="blog-sidebar-tags">
            @foreach($posttags as $posttag)
              @if($post->id == $posttag->post_id)
                <a href="#" class="tag def-btn btn-outline">{{ $posttag->tag->name }}</a>
              @endif
            @endforeach 
            </div><!--/.blog-sidebar-tags -->
          </div><!--/.content -->
        </div><!--/.content-row -->
      </div><!--/.blog-sidebar -->
    </div>
    <!-- Blog Post End -->
@endsection