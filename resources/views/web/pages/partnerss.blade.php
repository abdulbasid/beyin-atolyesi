@extends('layouts.outside')

@section('content')

<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs breadcrumbs-3">
                    <h2 class="page-title">İş Ortaklar</h2>
                    <ul>
                        <li>
                            <a  href="{{ route('welcome')}}">Anasayfa</a>
                        </li>
                        <li>
                            <a class="active" href="#">İş Ortakları</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
        

<div class="portfolio-area ptb-90">
    <div class="container">	
        <div class="row portfolio-style-2">
            <div class="grid" style="position: relative; height: 768.312px;">
            @foreach($partners as $partner)
                <div class="col-md-4 col-sm-6 col-xs-12 grid-item mix1 mb-30" style="position: absolute; left: 0%; top: 0px;">
                    <div class="portfolio hover-style1">
                        <div class="portfolio-img">
                            @if($partner->file)
                                <img alt="{{ $partner->name }}" src="{{ $partner->file }}"  class="fullwidth"  style="height: 250px!important;"/>
                            @endif
                        </div>
                        <div class="text-center title-color-2 pro-style-8">
                            <h3><a href="{{ route('partner', $partner->slug) }}">{{ $partner->name }}</a></h3>
                           
                        </div>									
                    </div>
                </div>					
                @endforeach
            </div>		
        </div>

    </div>
</div>

@endsection
