@extends('layouts.outside')

@section('content')
<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs breadcrumbs-3">
                    <h2 class="page-title">Blog</h2>
                    <ul>
                        <li>
                            <a  href="{{route('welcome')}}">Anasayfa</a>
                        </li>
                        <li>
                            <a class="active">Blog</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
		<!-- breadcrumbs end -->
        <!-- blog-area start -->
<section class="blog-area ptb-90 blog-leftbar">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="blog-right-sidebar blog-res2">
                        
                        <div class="blog-right-sidebar-top mb-60">
                            <h3 class="leave-comment-text">Son Yazılar</h3>
                            <div class="recent-all">
                            @foreach($lastarticles as $article)
                                <div class="recent-img-text mb-20">
                                    <div class="recent-img">
                                        <a href="{{ route('article', $article->slug) }}"><img src="{{ asset('web/img/blog/5.jpg') }}" alt=""></a>
                                    </div>
                                    <div class="recent-text">
                                        <h4>
                                            <a href="{{ route('article', $article->slug) }}">{{ $article->name }}</a>
                                        </h4>
                                        <p>{{ $article->team->name }}</p>
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                        
                       
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="row">
                    @foreach($articles as $article)
                        <div class="col-md-12 col-sm-6">
                            <div class="blog-text mb-30">
                                <div class="blog-img">
                                @if($article->file)
                                    <img alt="{{ $article->name }}" src="{{ $article->file }}"  class="fullwidth" height="250px'important;" style=";object-fit:cover"/>
                                @endif
                                    <div class="blog-view">
                                        <a href="{{ route('article', $article->slug) }}">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="blog-info white-bg">
                                    <div class="blog-meta">
                                        <div class="blog-meta-left">
                                            <span>
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                {{ $article->created_at }}
                                            </span>
                                            <span class="blog-middel">
                                                <i class="fa fa-user"></i>
                                                {{ $article->team->name }}
                                            </span>
                                        </div>
                                        
                                    </div>
                                    <div class="blog-info-top">
                                        <h3><a href="{{ route('article', $article->slug) }}">{{ $article->name }}</a></h3>
                                        {{ substr($article->body,0,250)  }}.
                                        <a href="{{ route('article', $article->slug) }}">
                                            Devamı..
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                  {{ $articles->render() }}
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

    

   
 
@endsection