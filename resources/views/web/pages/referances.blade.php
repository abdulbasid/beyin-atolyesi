@extends('layouts.outside')

@section('content')

	<!-- breadcrumbs start -->
    <section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="breadcrumbs breadcrumbs-3">
                            <h2 class="page-title">Referanslar</h2>
                            <ul>
                                <li>
                                    <a  href="{{ route('welcome')}}">Anasayfa</a>
                                </li>
                                <li>
                                    <a class="active" href="{{ route('web.pages.referances') }}">Referanslar</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<!-- breadcrumbs end -->
        <!-- portfolio area start -->
		<div class="portfolio-area ptb-90">
			<div class="container">
				<div class="portfolio-menu portfolio-left-menu text-center mb-50">
					<button class="active" data-filter="*">TÜMÜ</button>
                    @foreach($refcategories as $refcategory)
                        <button data-filter=".{{ $refcategory->slug }}">{{ $refcategory->name }} </button>
                    @endforeach
				</div>			
				<div class="row portfolio-style-2">
					<div class="grid">

                    @foreach($referances as $ref)
						<div class="col-md-3 col-sm-6 col-xs-12 grid-item {{ $ref->category->slug }} mb-30">
							<div class="portfolio hover-style1">
								<div class="portfolio-img">
                                @if($ref->file)
                                    <img alt="{{ $ref->name }}" src="{{ $ref->file }}"  class="fullwidth"  style="height: 250px!important;"/>
                                @endif
									<div class="portfolio-view">
										<a class="img-poppu" href="{{asset('web/img/portfolio/equal/1.jpg') }}">
											<i class="icon-focus"></i>
										</a>								
									</div>
								</div>
								<div class="portfolio-title-2 text-center title-color-2">
									<h3><a href="{{ route('referance', $ref->slug) }}">{{ $ref->name }}</a></h3>
									<span>{{ substr($ref->body,0,150)  }}...</span>
								</div>									
							</div>
                        </div>	
                    @endforeach				
							
						
					</div>		
				</div>
			
			</div>
		</div>		
        <!-- portfolio area end -->
        


@endsection
