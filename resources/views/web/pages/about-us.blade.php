@extends('layouts.outside')

@section('content')
<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="breadcrumbs breadcrumbs-3">
                        <h2 class="page-title">Hakkımızda</h2>
                        <ul>
                            <li>
                                <a  href="{{ route('welcome')}}">Anasayfa</a>
                            </li>
                            <li><a class="active" href="#">Hakkımızda</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="portfolio-details ptb-90">
            <div class="container">
                <div class="row">
                </div>
				<div class="row pt-60">
                @foreach($aboutuses as $aboutus)
					<div class="col-md-8">
						<div class="project-desc">
							<h3 class="desc">{{ $aboutus->title }}</h3><br>
							{{ $aboutus->body }}
						</div>
										
					</div>
					<div class="col-md-4">
						<div class="portfolio-meta">
                            @if($aboutus->file)
                                <img alt="{{ $aboutus->name }}" src="{{ $aboutus->file }}"  class="fullwidth" width="100%;"/>
                            @endif
						</div>					
                    </div>
                @endforeach
				</div>
                
            </div>
        </section>

    
    <!-- service-area start -->
    <div class="service-area border-top white-bg ptb-90">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                        
                        <h2>Nasıl Yapıyoruz ?</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa.</p>
                    </div>
                </div>
            </div>
            <div class="row text-center">
            @foreach($howdoes as $howdo)
                <div class="col-md-4 col-sm-6">
                    <div class="single-service white-bg mb-30">
                        <i class="fa fa-object-ungroup"></i>
                        <div class="meta-services">
                        
                            <h3>{{ $howdo->title }}</h3>
                            
                            {{ $howdo->body }}
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>

    @include('web.home.info-counter')
    

    @include('web.home.team-slider')


@endsection
