@extends('layouts.outside')

@section('content')
<!-- Banner Small Begin -->
 <section class="section banner-small parallax-section valign-wrap" style="background: url('{{ asset('web/images/banner-small-3.jpg') }}'); background-size: cover;">
      <div class="overlay"></div>
      <div class="content-wrap valign-bottom">
        <div class="container">
          <div class="col-md-12">
            <div class="title wow animated fadeInUp"><h1>Gallery List</h1></div>
            <div class="subtitle wow animated fadeIn"><h2>Photograph Of Our Trip</h2></div>            
          </div>
        </div>
      </div><!--/.content-wrap -->
    </section><!--/.banner-small -->
    <!-- Banner Small End -->

    <!-- Gallery Filter Begin -->
    <section class="portfoliofilter wow animated fadeIn text-center sec-pad-t">
      <div class="container">
        <div class="row">
          <a href="javascript:void(0)" data-filter="*" class="current filterbutton">
            All Categories
          </a>
          <a href="javascript:void(0)" data-filter=".tripPhoto" class="filterbutton">
            Trip Photography
          </a>
          <a href="javascript:void(0)" data-filter=".placePhoto" class="filterbutton">
            Places Photography
          </a>
        </div>
      </div>
    </section>
    <!-- Gallery Filter End -->

    <!-- Gallery List Begin -->
    <section class="gallery-list">
      <div class="container no-h-padding">
        <div class="isotope-wrap masonry-grid grouped-image-list">

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space tripPhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-1.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-1.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space placePhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-2.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-2.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space placePhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-3.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-3.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space tripPhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-4.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-4.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space placePhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-5.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-5.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space tripPhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-6.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-6.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space placePhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-7.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-7.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

          <div class="element-item masonry-item col-md-3 col-sm-6 col-xs-12 with-space tripPhoto">
            <figure class="gallery-column margin-responsive">
              <img src="{{ asset('web/images/gallery-image-8.jpg') }}" alt="">

              <figcaption>
                <div class="valign-wrap fullwidth fullheight">
                  <div class="caption-wrap valign-middle">
                    <a href="{{ asset('web/images/gallery-image-8.jpg') }}" class="no-effect"><i class="pe-7s-search pe-2x"></i></a>
                  </div><!--/.caption-wrap -->
                </div>
              </figcaption>
            </figure><!--/.gallery-column -->
          </div>

        </div><!--/.isotope-wrap -->
      </div><!--/.container -->
    </section><!--/.gallery-list -->
    <!-- Gallery List End -->

    <!-- Pagination Section Begin -->
    <div class="container">
      <div class="pagination-wrap wow animated fadeIn sec-hq-pad-b sec-q-pad-t">
        <ul class="pagination">
          <li>
            <a href="#" aria-label="Previous">
              <span aria-hidden="true">Prev</span>
            </a>
          </li>
          <li><a href="#" class="active">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li>
            <a href="#" aria-label="Next">
              <span aria-hidden="true">Next</span>
            </a>
          </li>
        </ul><!--/.pagination -->
      </div><!--/.pagination-wrap -->
    </div>
    <!-- Pagination Section End -->

    @endsection