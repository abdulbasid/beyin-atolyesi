@extends('layouts.outside')

@section('content')
    
    <section class="section banner-small parallax-section valign-wrap" style="background: url('{{ asset('web/images/banner-small-2.jpg') }}'); background-size: cover;">
      <div class="overlay"></div>
      <div class="content-wrap valign-bottom">
        <div class="container">
          <div class="col-md-12">
            <div class="title wow animated fadeInUp"><h1>Our Travel Portfolio</h1></div>
            <div class="subtitle wow animated fadeIn"><h2>Our Best Travel Services</h2></div>            
          </div>
        </div>
      </div>
    </section>

	<section class="portfoliofilter wow animated fadeIn text-center sec-pad-t">
      <div class="container">
       
      </div>
    </section>
  
    <section class="portfolio-collase">
      <div class="container no-h-padding">
        <div class="isotope-wrap masonry-grid">

        @foreach($teams as $team)
        <div class="element-item masonry-item col-md-4 col-sm-6 col-xs-12 with-space seaAdventure">
            <div class="portfolio-item">
              <figure class="portfolio-figure">
                <img src="{{ asset('web/images/portfolio-1.jpg') }}" alt="Portfolio Image">
                <figcaption>
                  <div class="valign-wrap fullwidth fullheight">
                    <div class="caption-wrap valign-middle">
                      <h2>{{ $team->name }}</h2>
                      <div class="separator"></div>
                      <p>{{ $team->unvan }}</p>
                      <a href="{{ route('team', $team->slug) }}" class="def-btn btn-outline portfolio-btn">
                        <span class="text-content">
                        Detay <i class="pe-7s-angle-right-circle"></i></span>
                      </a>
                    </div>
                  </div>
                </figcaption>
              </figure>
            </div>
          </div>
        @endforeach

          

        </div>
      </div>
    </section>
    

 
    <section class="title-paragraph-center text-center">
      <div class="container">
        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3 margin-responsive">
          <h4 class="text-uppercase sec-h-pad-t">We'd love to hear about your trip request</h4>
          <div class="col-md-12 nopad-h sec-q-pad-t sec-pad-b">
           
          </div>
        </div>
      </div>
    </section>
 

    
	@include('web.home.logos')

 
@endsection