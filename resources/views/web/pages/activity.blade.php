@extends('layouts.outside')

@section('content')
    <!-- Banner Small Begin -->
    <section class="section banner-small parallax-section valign-wrap" style="background: url('{{ ('web/images/banner-small-3.jpg') }}') }}'); background-size: cover;">
      <div class="overlay"></div>
      <div class="content-wrap valign-bottom">
        <div class="container">
          <div class="col-md-12">
            <div class="title"><h1>Etkinliklerimiz</h1></div>
            <div class="subtitle"><h2>List of Interesting News</h2></div>       
          </div>
        </div>
      </div><!--/.content-wrap -->
    </section><!--/.banner-small -->
    <!-- Banner Small End -->

   
    <div class="container">
      <div class="col-md-8 sec-pad-t">
      

        @foreach($posts as $post)
        <article class="blog-post col-md-12 no-h-padding wow animated fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
          <div class="image-content col-md-12 no-h-padding">
            <div class="image">
              <img src="{{ asset('web/images/blog-list-img-1.jpg') }}" alt="">
            </div>
            <div class="col-lg-5 col-md-4 col-sm-4 date">Oct 5, 2015</div>
            <div class="col-lg-7 col-md-8 col-sm-8 info text-right">
              <a href="" class="link">
                <i class="pe-7s-like"></i>
                30 Likes
              </a>
              <a href="" class="link">
                <i class="pe-7s-link"></i>
                7 Shared
              </a>
              <a href="" class="link">
                <i class="pe-7s-comment"></i>
                12 Comments
              </a>
            </div>
          </div><!--/.image-content -->

          <div class="text-content col-md-12 no-h-padding margin-responsive">
            <div class="title">
              <a target="blank" href="{{ route('post', $post->slug) }}"><h3>{{ $post->name }}</h3></a>
            </div>
            <div class="cat-name text-left">
              <a href="" class="link">Türkiye</a> /
              <a href="" class="link">{{ $post->city }}</a>
            </div>
            <div class="text">
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the standard dummy text.
              </p>
            </div>
            <div class="button-wrap">
                
              <a target="blank" href="{{ route('post', $post->slug) }}" class="def-btn btn-outline">Detay</a>
            </div>
          </div><!--/.text-content -->
        </article>
          
        @endforeach

   

       
        {{ $posts->render() }}
       
        <!-- <div class="pagination-wrap">
          <ul class="pagination">
            <li>
              <a href="#" aria-label="Previous">
                <span aria-hidden="true">Prev</span>
              </a>
            </li>
            <li><a href="#" class="active">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">Next</span>
              </a>
            </li>
          </ul>
        </div> -->
       
      </div>
      
      <div class="col-md-4 blog-sidebar sec-pad-t">

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Son Etkinlikler</h3>
          </div>

          <div class="content">
            @foreach($lastposts as $post)
                <div class="media blog-sidebar-list">
                <div class="media-left">
                    <a href="{{ route('post', $post->slug) }}">
                    <img class="media-object" src="{{ ('web/images/blog-sidebar-img-1.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><a href="{{ route('post', $post->slug) }}">{{ $post->name }}</a></h4>
                    <p>Türkiye / {{ $post->city }}</p>
                </div>
                </div>
            @endforeach

          </div><!--/.content -->
        </div><!--/.content-row -->

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Popular Post</h3>
          </div>

          <div class="content">
            <ul class="blog-sidebar-list list-unstyled">
              <li><a href="#">Explore Wonderful bali island</a></li>
              <li><a href="">Diving in Maldives</a></li>
              <li><a href="">Bunaken diving spot</a></li>
              <li><a href="">BSail to Hawaii discount</a></li>
            </ul><!--/.blog-sidebar-list -->
          </div><!--/.content -->
        </div><!--/.content-row -->

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Archives</h3>
          </div>

          <div class="content">
            <ul class="blog-sidebar-list list-unstyled">
              <li><a href="#">January (4)</a></li>
              <li><a href="#">February (4)</a></li>
              <li><a href="#">March (4)</a></li>
              <li><a href="#">April (4)</a></li>
              <li><a href="#">May (4)</a></li>
            </ul><!--/.blog-sidebar-list -->
          </div><!--/.content -->
        </div><!--/.content-row -->

        <div class="content-row wow animated fadeIn">
          <div class="blog-sidebar-title">
            <h3 class="underlined-heading">Blog Tags</h3>
          </div>

          <div class="content">
            <div class="blog-sidebar-tags">
              <a href="#" class="tag def-btn btn-outline">Diving</a>
              <a href="#" class="tag def-btn btn-outline">Ship</a>
              <a href="#" class="tag def-btn btn-outline">Travel</a>
              <a href="#" class="tag def-btn btn-outline">Trip</a>
              <a href="#" class="tag def-btn btn-outline">Island</a>
              <a href="#" class="tag def-btn btn-outline">Sea</a>
            </div><!--/.blog-sidebar-tags -->
          </div><!--/.content -->
        </div><!--/.content-row -->
      </div><!--/.blog-sidebar -->
    </div>
    <!-- Blog Post End -->
@endsection