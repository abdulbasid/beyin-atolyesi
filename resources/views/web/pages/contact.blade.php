@extends('layouts.outside')

@section('content')
<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs breadcrumbs-3">
                    <h2 class="page-title">Bize Ulaşın</h2>
                    <ul>
                        <li>
                            <a href="{{ route('welcome')}}">Anasayfa</a>
                        </li>
                        <li>
                            <a class="active">İletişim</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumbs end -->
<div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="section-title">
                       
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-sm-6">
                    <div class="single-service white-bg mb-30">
                        <i class="fa fa-map-marker"></i>
                        <div class="meta-services">
                            <h3>Adres</h3>
                            @foreach($contacts as $contact) 
                            <p class="text-center">
                                {{$contact->address}}
                                <br>
                                {{$contact->address2}}
                            </p>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="single-service white-bg mb-30">
                        <i class="fa fa-phone"></i>
                        <div class="meta-services">
                            <h3>Telefon</h3>
                            @foreach($contacts as $contact) 
                            <p class="text-center">
                                {{$contact->gsm1}}
                                <br>  
                                {{$contact->gsm2}}
                                
                            </p>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hidden-sm">
                    <div class="single-service white-bg mb-30">
                        <i class="fa fa-envelope"></i>
                        <div class="meta-services">
                            <h3>E-Mail</h3>
                            @foreach($contacts as $contact) 
                            <p class="text-center">
                                {{$contact->email}}
                                <br>    
                                <br>  
                                {{$contact->email2}}
                            </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-md-12 col-sm-12">
                <div class="contact-area-all">
                @foreach($contacts as $contact) 
                <iframe src="{{$contact->map}}" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                @endforeach
                </div>
            </div>
        </div>
    </div>

<section class="contact-area ptb-90">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-md-offset-2">
                <div class="conract-area-bottom">
                    <h3 class="desc">Mesaj Gönder</h3>
                    <form class="contact_form" id="contact_form" action="http://nahartheme.com/tf/funds-preview/funds/contact-form.php" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="main-input">
                                    <input id="contact_name" name="name" placeholder="Ad*" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="main-input mrg-eml mrg-contact">
                                    <input type="email" name="email" id="contact_email" placeholder="Email*">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="main-input mt-20 mb-20">
                                    <input id="contact_subject" name="subject" placeholder="Başlık*" type="text">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-leave2">
                                    <textarea name="message" id="contact_message" placeholder="Mesajınız"></textarea>
                                    <button class="submit ripple-btn" type="submit" name="submit" id="contact_submit" data-complete-text="Well done!">Gönder</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
       
    </div>
    
</section>



 
@endsection