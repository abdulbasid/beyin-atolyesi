
<script src="{{ asset('web/js/vendor/jquery-1.12.0.min.js') }}"></script>
<script src="{{ asset('web/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('web/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('web/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('web/js/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('web/js/jquery.meanmenu.js') }}"></script>
<script src="{{ asset('web/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('web/js/plugins.js') }}"></script>
<script src="{{ asset('web/js/main.js') }}"></script>