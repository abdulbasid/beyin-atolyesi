
 <link rel="shortcut icon" type="image/x-icon" href="{{ asset('web/img/favicon.png') }}">
		
<!-- all css here -->
<link rel="stylesheet" href="{{ asset('web/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/et-line-fonts.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/meanmenu.css') }}">
<link rel="stylesheet" href="{{ asset('web/css/shortcode/shortcodes.css') }}">

<link rel="stylesheet" href="{{ asset('web/css/responsive.css') }}">
<script src="{{ asset('web/js/vendor/modernizr-2.8.3.min.js') }}"></script>