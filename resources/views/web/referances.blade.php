<!-- breadcrumbs start -->

<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs breadcrumbs-3">
                    <h2 class="page-title">Cotact Us</h2>
                    <ul>
                        <li>
                            <a class="active" href="index.html">Home</a>
                        </li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
    <!-- breadcrumbs end -->
    <!-- portfolio area start -->
    <div class="portfolio-area ptb-90">
        <div class="container">
            <div class="portfolio-menu portfolio-left-menu text-center mb-50">
                <button class="active" data-filter="*">ALL</button>
                <button data-filter=".mix1">Graphic </button>
                <button data-filter=".mix2">Photography</button>
                <button data-filter=".mix3">Coding </button>
                <button data-filter=".mix4">Branding </button>
            </div>			
            <div class="row portfolio-style-2">
                <div class="grid">
                @foreach($referances as $ref)
                    <div class="col-md-3 col-sm-6 col-xs-12 grid-item mix1 mb-30">
                        <div class="portfolio hover-style1">
                            <div class="portfolio-img">
                                @if($ref->file)
                                    <img alt="{{ $ref->name }}" src="{{ $ref->file }}" id="fullwidth" />
                                @endif
                                <div class="portfolio-view">
                                    <a class="img-poppu" href="{{ route('referance', $ref->slug) }}" >
                                        <i class="icon-focus"></i>
                                    </a>								
                                </div>
                            </div>
                            <div class="portfolio-title-2 text-center title-color-2">
                                <h3><a href="#">Web Development</a></h3>
                                <span>Design, Print</span>
                            </div>									
                        </div>
                    </div>					
                @endforeach
                </div>		
            </div>
        </div>
    </div>		
    <!-- portfolio area end -->
