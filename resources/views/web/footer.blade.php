<footer class="footer-area ptb-20 gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="copyright">
                    <p>Copyright © 2019 <a href="https://beyinatolyesi.com/">beyin-atolyesi.com</a></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="footer-icon">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>