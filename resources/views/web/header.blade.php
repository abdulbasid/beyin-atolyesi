<header>
    <div class="header-area header-area-4 header-sticky" >
        <div class="container" style="margin-top: 10px;">
            <div class="row">
                <div class="col-md-4 col-xs-12" style="display: inline-block; vertical-align: middle;">
                    <div class="logo">
                        <a href="{{ route('welcome') }}">
                        <div style="height: 90px; background-repeat: no-repeat;">  </div>
                        <!--<img src="img/logo.png" title="Beyin Atölyesi" alt="Beyin Atölyesi Logo">-->
                        </a>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12" style="display: inline-block; vertical-align: middle;" >
                    <div class="main-menu text-right">
                        <nav>
                            <ul class="menu">
                                <li><a  href="{{ route('web.pages.about-us') }}">BİZ KİMİZ?</a></li>
                                
                                <li><a href="#">Neler Yapıyoruz?</a>
                                    <ul>

                                @foreach($whatwedoes as $whatwedo)
                                    <li><a href="{{ route('whatwedo', $whatwedo->slug) }}">{{ $whatwedo->title }}</a></li>
                                @endforeach
                                      
                                        
                                    </ul>
                                </li>                                   
                                </li>
                                <li><a href="{{ route('web.pages.partnerss') }}">İş Ortaklarımız</a></li>
                                <li><a href="{{ route('web.pages.referances') }}">Referanslar</a>   
                                <li><a href="{{ route('web.pages.articles') }}">Blog</a></li>
                                <li><a href="{{ route('web.pages.contact') }}">Bİze Ulaşın!</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    </header>