@extends('layouts.outside')

@section('content')

<section class="breadcrumbs-area ptb-100 port bread-card pattern-bread gray-bg border-bread">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="breadcrumbs breadcrumbs-3">
                    <h2 class="page-title">{{ $partner->name }}</h2>
                    <ul>
                        <li><a href="{{ route('welcome')}}">Anasayfa</a></li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumbs end -->
<section class="portfolio-details ptb-90">
    <div class="container">
    <div class="row">
            <div class="col-md-12">
                
                @if($partner->file)
                    <img alt="{{ $partner->name }}" src="{{ $partner->file }}"  class="fullwidth" width="100%"/>
                @endif
            
            </div>
        </div>
        <div class="row pt-60">
            <div class="col-md-8">
                <div class="project-desc">
                    <h3 class="desc">Açıklama</h3><br>
                    {{ $partner->body }}
                </div>
    
                <div class="post-share">
                    <ul>
                        <li>Paylaş : </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>						
            </div>
            <div class="col-md-4">
                
                <div class="blog-right-sidebar-bottom">
                    <h3 class="leave-comment-text">Tags</h3>
                    <ul>
                    @foreach($partnertags as $partnertag)
                        @if($partner->id == $partnertag->partner_id)
                        <li><a href="#">#{{ $partnertag->tag->name }}</a></li>
                        @endif
                    @endforeach 
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

        
@endsection