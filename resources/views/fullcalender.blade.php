@extends('layouts.inside')

@section('content')

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
  
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                  Etkinlik Takvimi
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="#" class="btn btn-clean btn-icon-sm">
                        <i class="la la-long-arrow-left"></i>
                        Geri
                    </a>
                    <a href="" class="btn btn-brand btn-elevate">
                        <i class="la la-plus"></i>
                        Etkinlik Ekle
                    </a>
                    &nbsp;
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

        </div>
        <div class="kt-portlet__body kt-portlet__body--fit">

            <!--begin: Datatable -->
            
            <div class="panel-body">
                    {!! $calendar->calendar() !!}
                </div>
            <!--end: Datatable -->
        </div>
    </div>
</div>




<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
 {!! $calendar->script() !!}
   

@endsection