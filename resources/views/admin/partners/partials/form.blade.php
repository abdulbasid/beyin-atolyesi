{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="row">
    <div class="col-md-6">
        <br>

        <div class="form-group">
			{{ Form::label('status', 'Anasayfada') }}
			<label>
				{{ Form::radio('status', 'active') }} Göster
			</label>
			<label>
				{{ Form::radio('status', 'passive') }} Gizle
			</label>
        </div>
    
        <div class="form-group">
            {{ Form::label('name', 'İş Ortağı Adı : ',['class' => ' col-form-label']) }}
            {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
        </div>

        <div class="form-group">
            {{ Form::label('slug', 'URL : ',['class' => ' col-form-label']) }}
            {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
        </div>

        <div class="form-group">
            {{ Form::label('image', 'Resim : ',['class' => ' col-form-label']) }}
            {{ Form::file('image') }}
        </div>

        <div class="form-group">
			{{ Form::label('tags', 'Etiketler') }}
			<div>
				
			@foreach($tags as $tag)
				<label>
					{{ Form::checkbox('tags[]', $tag->id) }} {{ $tag->name }}
				</label>
			@endforeach
			</div>
			
        </div>
    </div>

    <div class="col-md-6">
        <br>
        <div class="form-group">
            {{ Form::label('body', 'Açıklama : ',['class' => ' col-form-label']) }}
            {{ Form::textarea('body', null, ['class' => 'form-control', 'id' => 'body']) }}
        </div>

        
    </div>

</div>


<div class="form-group">
    {{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
</div>

