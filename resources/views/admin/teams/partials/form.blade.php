{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="row">
    <div class="col-md-6">
        <br>
        <div class="form-group">
        {{ Form::label('name', 'İsim Soyisim') }}
        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
        </div>
        <div class="form-group">
            {{ Form::label('slug', 'URL') }}
            {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
        </div>
        <div class="form-group">
            {{ Form::label('email', 'E-Mail') }}
            {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) }}
        </div>
        <div class="form-group">
            {{ Form::label('phone', 'Telefon') }}
            {{ Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']) }}
        </div>
        <div class="form-group">
            {{ Form::label('job', 'Meslek') }}
            {{ Form::text('job', null, ['class' => 'form-control', 'id' => 'job']) }}
        </div>
        <div class="form-group">
            {{ Form::label('unvan', 'Unvan') }}
            {{ Form::text('unvan', null, ['class' => 'form-control', 'id' => 'unvan']) }}
        </div>
        <div class="form-group">
        {{ Form::label('city', 'Şehir') }}
        {{ Form::text('city', null, ['class' => 'form-control', 'id' => 'city']) }}
        </div>
        <div> <h4>Sosyal Medya</h4> </div> <br/>
        <div class="form-group">
            {{ Form::label('instagram', 'İnstagram') }}
            {{ Form::text('instagram', null, ['class' => 'form-control', 'id' => 'instagram']) }}
        </div>
        <div class="form-group">
            {{ Form::label('twitter', 'Twitter') }}
            {{ Form::text('twitter', null, ['class' => 'form-control', 'id' => 'twitter']) }}
        </div>
        <div class="form-group">
            {{ Form::label('facebook', 'Facebook') }}
            {{ Form::text('facebook', null, ['class' => 'form-control', 'id' => 'facebook']) }}
        </div>
    </div>

    <div class="col-md-6">
        <br>
        <div class="form-group">
        {{ Form::label('image', 'Resim') }}
        {{ Form::file('image') }}
        </div>

        <div class="form-group">
        {{ Form::label('body', 'Biyografi') }}
        {{ Form::textarea('body', null, ['class' => 'form-control', 'id' => 'body']) }}
        </div>

      

    
    </div>
</div>









<div class="form-group">
{{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
</div>

