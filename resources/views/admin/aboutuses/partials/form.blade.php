{{ Form::hidden('user_id', auth()->user()->id) }}


<div class="row">
    <div class="col-md-6">
        <br>
        <div class="form-group">
        {{ Form::label('title', 'Başlık') }}
        {{ Form::text('title', null, ['class' => 'form-control', 'id' => 'title']) }}
        </div>
        <div class="form-group">
            {{ Form::label('body', 'Açıklama') }}
            {{ Form::textarea('body', null, ['class' => 'form-control', 'id' => 'body']) }}
        </div>
        <div class="form-group">
            {{ Form::label('image', 'Resim') }}
            {{ Form::file('image') }}
        </div>

        <div class="form-group">
        {{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
        </div>
    </div>
</div>



