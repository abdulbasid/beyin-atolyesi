{{ Form::hidden('user_id', auth()->user()->id) }}


<div class="row">
	<div class="col-md-6">
		<br>
		<div class="form-group">
		{{ Form::label('category_id', 'Kategori') }}
		{{ Form::select('category_id', $categories, null, ['class' => 'form-control']) }}
		</div> 
		<div class="form-group">
			{{ Form::label('name', 'Etkinlik Başlığı') }}
			{{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
		</div>
		<div class="form-group">
			{{ Form::label('slug', 'URL') }}
			{{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
		</div>

		<div class="form-group">
			{{ Form::label('city', 'Şehir') }}
			{{ Form::text('city', null, ['class' => 'form-control', 'id' => 'city']) }}
		</div>

		<div class="form-group">
			{{ Form::label('address', 'Adres') }}
			{{ Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) }}
		</div>

		<div class="form-group">
		{{ Form::label('startdate', 'Başlangıç Tarihi') }}
		{{ Form::date('startdate', null, ['class' => 'form-control', 'id' => 'startdate']) }}
		</div>

		<div class="form-group">
			{{ Form::label('enddate', 'Bitiş Tarihi') }}
			{{ Form::date('enddate', null, ['class' => 'form-control', 'id' => 'enddate']) }}
		</div>

		

	</div>

	<div class="col-md-6">
	<br>
		<div class="form-group">
			{{ Form::label('image', 'Resim') }}
			{{ Form::file('image') }}
		</div>

		<div class="form-group">
			{{ Form::label('tags', 'Etiketler') }}
			<div>
				
			@foreach($tags as $tag)
				<label>
					{{ Form::checkbox('tags[]', $tag->id) }} {{ $tag->name }}
				</label>
			@endforeach
			</div>
			
		</div>

		<div class="form-group">
			{{ Form::label('teams', 'Kişiler') }}
			<div>
				
			@foreach($teams as $team)
				<label>
					{{ Form::checkbox('teams[]', $team->id) }} {{ $team->name }}
				</label>
			@endforeach
			</div>
			
		</div>
		
		<div class="form-group">
			{{ Form::label('body', 'Açıklama') }}
			{{ Form::textarea('body', null, ['class' => 'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('status', 'Durum') }}
			<label>
				{{ Form::radio('status', 'active') }} Aktif
			</label>
			<label>
				{{ Form::radio('status', 'passive') }} Pasif
			</label>
		</div>

		<div class="form-group">
			{{ Form::submit('Ekle', ['class' => 'btn btn-sm btn-primary']) }}
		</div>
	</div>
</div>




@section('scripts')
<script src="{{ asset('vendor/stringToSlug/jquery.stringToSlug.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });

	    CKEDITOR.config.height = 400;
		CKEDITOR.config.width  = 'auto';

		CKEDITOR.replace('body');
	});
</script>
@endsection