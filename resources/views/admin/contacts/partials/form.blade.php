{{ Form::hidden('user_id', auth()->user()->id) }}

<div class="row">
    <div class="col-md-6">
        <br>
        <div class="form-group">
            {{ Form::label('address', 'Adres : ') }}
            {{ Form::text('address', null, ['class' => 'form-control', 'id' => 'address']) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'E-Mail : ') }}
            {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email']) }}
        </div>

        <div class="form-group">
            {{ Form::label('gsm', 'Telefon : ') }}
            {{ Form::text('gsm', null, ['class' => 'form-control', 'id' => 'gsm']) }}
        </div>

        <div class="form-group">
            {{ Form::label('gsm2', 'Telefon 2 : ') }}
            {{ Form::text('gsm2', null, ['class' => 'form-control', 'id' => 'gsm2']) }}
        </div>
    </div>

    <div class="col-md-6">
        <br>
        <div class="form-group">
            {{ Form::label('map', 'Harita : ') }}
            {{ Form::textarea('map', null, ['class' => 'form-control', 'id' => 'map']) }}
            ** https:// ile başlayan linki yapıştırınız.
        </div>
    </div>
</div>





<!-- <div class="form-group">
    {{ Form::label('image', 'Resim : ',['class' => ' col-form-label']) }}
    {{ Form::file('image') }}
</div> -->

<div class="form-group">
    {{ Form::submit('Ekle', ['class' => 'btn btn-primary btn-wide col-md-12 ']) }}
</div>

