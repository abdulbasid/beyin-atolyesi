<div class="row">
    <div class="col-md-6">
        <br>
        <div class="form-group">
        {{ Form::label('name', 'Kategori Adı') }}
        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
        </div>
        <div class="form-group">
            {{ Form::label('slug', 'URL') }}
            {{ Form::text('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
        </div>
        <div class="form-group">
            {{ Form::label('body', 'Açıklama') }}
            {{ Form::textarea('body', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::submit('Ekle', ['class' => 'btn btn-sm btn-primary']) }}
        </div>
    </div>
</div>



@section('scripts')
<script src="{{ asset('vendor/stringToSlug/jquery.stringToSlug.min.js') }}"></script>
<script>
	$(document).ready(function(){
	    $("#name, #slug").stringToSlug({
	        callback: function(text){
	            $('#slug').val(text);
	        }
	    });
	});
</script>
@endsection