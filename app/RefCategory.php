<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefCategory extends Model
{
    protected $table = 'refcategories';
    protected $fillable = [
        'name', 'slug', 'body'
    ];

    public function referance()
    {
        return $this->hasMany(Referance::class);
    }
}
