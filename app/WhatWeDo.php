<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatWeDo extends Model

{

    protected $table = 'whatwedoes';

    protected $fillable = [
         'main_title','title','body','slug','file'
    ];


  
}
