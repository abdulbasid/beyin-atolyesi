<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referance extends Model
{
    protected $table = 'referances';
    protected $fillable = [
       'category_id','body','name','slug','status','file'
    ];

    public function category()
    {
        return $this->belongsTo(RefCategory::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}

