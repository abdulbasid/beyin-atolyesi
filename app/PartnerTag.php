<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerTag extends Model
{
    protected $table = 'partner_tag';

    protected $fillable = [
        'partner_id', 'tag_id'
    ];

    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
