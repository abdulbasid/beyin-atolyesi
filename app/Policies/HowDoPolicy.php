<?php

namespace App\Policies;

use App\HowDo;
use Illuminate\Auth\Access\HandlesAuthorization;

class HowDoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(User $user, HowDo $howdo)
    {
        return $howdo->id;
    }
}
