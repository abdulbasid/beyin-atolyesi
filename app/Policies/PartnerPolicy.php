<?php

namespace App\Policies;

use App\Partner;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReferancePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(Partner $partner)
    {
        return $partner->id;
    }
}
