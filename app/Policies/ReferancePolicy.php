<?php

namespace App\Policies;

use App\Referance;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReferancePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(Referance $referance)
    {
        return $referance->id;
    }
}
