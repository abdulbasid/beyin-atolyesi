<?php

namespace App\Policies;

use App\WhatWeDo;
use Illuminate\Auth\Access\HandlesAuthorization;

class WhatWeDoPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(User $user, WhatWeDo $whatwedo)
    {
        return $whatwedo->id;
    }
}
