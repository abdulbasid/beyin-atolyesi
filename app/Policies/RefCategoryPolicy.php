<?php

namespace App\Policies;

use App\RefCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class RefCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function pass(RefCategory $refcategory)
    {
        return $refcategory->id;
    }
}
