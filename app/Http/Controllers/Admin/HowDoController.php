<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\HowDoStoreRequest;
use App\Http\Requests\HowDoUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\HowDo;

class HowDoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $howdoes = HowDo::orderBy('id', 'DESC')
            ->paginate();


        return view('admin.howdoes.index', compact('howdoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.howdoes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HowDoStoreRequest $request)
    {
        $howdo = HowDo::create($request->all());
      

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $howdo->fill(['file' => asset($path)])->save();
        }

      

        return redirect()->route('howdoes.index', $howdo->id)->with('info', ' Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $howdo = HowDo::find($id);
       

        return view('admin.howdoes.show', compact('howdo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $howdo       = HowDo::find($id);
        

        return view('admin.howdoes.edit', compact('howdo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HowDoUpdaterequest $request, $id)
    {
        $howdo = WhatWeDo::find($id);
       

        $howdo->fill($request->all())->save();

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $howdo->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('howdoes.index', $howdo->id)->with('info', 'Başarıyla Güncellendi');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $howdo =HowDo::where('id',$id)->first();


        if ($howdo != null) {
            $howdo->delete();
            return redirect()->route('howdoes.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('howdoes.index')->with(['info'=> 'Hatalı ID']);
        }
    
}
