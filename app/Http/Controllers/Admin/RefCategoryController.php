<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\RefCategoryStoreRequest;
use App\Http\Requests\RefCategoryUpdateRequest;

use App\Http\Controllers\Controller;

use App\RefCategory;

class RefCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $refcategories = RefCategory::orderBy('id', 'DESC')->paginate();

        return view('admin.refcategories.index', compact('refcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.refcategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RefCategoryStoreRequest $request)
    {
        $refcategory = RefCategory::create($request->all());

        return redirect()->route('refcategories.index', $refcategory->id)->with('info', 'Kategori Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $refcategory = RefCategory::find($id);

        return view('admin.refcategories.show', compact('refcategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $refcategory = RefCategory::find($id);

        return view('admin.refcategories.edit', compact('refcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RefCategoryUpdateRequest $request, $id)
    {
        $refcategory = RefCategory::find($id);

        $refcategory->fill($request->all())->save();

        return redirect()->route('refcategories.index', $refcategory->id)->with('info', 'Kategori Başarıyla Güncellendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $refcategory = RefCategory::find($id)->delete();

        return back()->with('info', 'Başarıyla Silindi');
    }
}
