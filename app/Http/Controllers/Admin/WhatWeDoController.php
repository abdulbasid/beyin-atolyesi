<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\WhatWeDoStoreRequest;
use App\Http\Requests\WhatWeDoUpdateRequest;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

use App\WhatWeDo;

class WhatWeDoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $whatwedoess = WhatWeDo::orderBy('id', 'DESC')
            ->paginate();


        return view('admin.whatwedoess.index', compact('whatwedoess'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.whatwedoess.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WhatWeDoStoreRequest $request)
    {
        $whatwedo = WhatWeDo::create($request->all());
      

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $whatwedo->fill(['file' => asset($path)])->save();
        }

      

        return redirect()->route('whatwedoess.index', $whatwedo->id)->with('info', ' Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $whatwedo = WhatWeDo::find($id);
       

        return view('admin.whatwedoess.show', compact('whatwedo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $whatwedo       = WhatWeDo::find($id);
        

        return view('admin.whatwedoess.edit', compact('whatwedo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WhatWeDoUpdaterequest $request, $id)
    {
        $whatwedo = WhatWeDo::find($id);
       

        $whatwedo->fill($request->all())->save();

        //IMAGE 
        if($request->file('image')){
            $path = Storage::disk('public')->put('image',  $request->file('image'));
            $whatwedo->fill(['file' => asset($path)])->save();
        }

        return redirect()->route('whatwedoess.index', $whatwedo->id)->with('info', 'Başarıyla Güncellendi');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $whatwedo =WhatWeDo::where('id',$id)->first();


        if ($whatwedo != null) {
            $whatwedo->delete();
            return redirect()->route('whatwedoess.index')->with(['info'=> 'Başarıyla Silindi']);
        }

        return redirect()->route('whatwedoess.index')->with(['info'=> 'Hatalı ID']);
        }
    
}
