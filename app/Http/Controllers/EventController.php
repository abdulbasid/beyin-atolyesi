<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Calendar;
use App\Post;
use App\DivingTraning;

class EventController extends Controller
{
    public function index()
    {
        $events = [];
        $data = Post::all();
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->name,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date.' +1 day'),
                    null,
                    // Add color and link on event
	                [
	                    'color' => '#33C1FF',
	                    'url' => 'pass here url and any route',
	                ]
                );
            }
        }
        $divingtranings = DivingTraning::All();
        $calendar = Calendar::addEvents($events);
        return view('fullcalender', compact('calendar','divingtranings'));
    }
}
