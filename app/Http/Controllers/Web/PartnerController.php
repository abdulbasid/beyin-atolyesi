<?php

namespace App\Http\Controllers\Web;

use App\Partner;
use App\WhatWeDo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index(){

        $partners = Partner::All();
        $whatwedoes = WhatWeDo::All();

    	return view('web.pages.partnerss', compact('partners','whatwedoes'));
    }
}
