<?php

namespace App\Http\Controllers\Web;

use App\Aboutus;
use App\Referance;
use App\Team;
use App\WhatWeDo;
use App\HowDo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){

        $aboutuses = Aboutus::All();
        $whatwedoes = WhatWeDo::All();
        $referances = Referance::All();
        $teams = Team::All();
        $howdoes = HowDo::All();

    	return view('web.pages.about-us', compact('aboutuses','referances','teams','whatwedoes','howdoes'));
    }
}
