<?php

namespace App\Http\Controllers\Web;

use App\Contact;
use App\Referance;
use App\WhatWeDo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){
        
        $contacts = Contact::All();
        $referances = Referance::All();
        $whatwedoes = WhatWeDo::All();

    	return view('web.pages.contact', compact('contacts','referances','whatwedoes'));
    }
}
