<?php

namespace App\Http\Controllers\Web;

use App\WhatWeDo;
use App\Referance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WhaWeDoController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){

        $referances = Referance::All();
        $whatwedoes = WhatWeDo::All();

    	return view('web.pages.whatwedoes', compact('referances','whatwedoes'));
    }

    public function whatwedo($slug){
        $referances = Referance::All();
        $whatwedo = WhatWeDo::where('slug', $slug)->first();
        $whatwedoes = WhatWeDo::All();
       

    	return view('web.whatwedo', compact('referances','whatwedo','whatwedoes'));
    }

    

}
