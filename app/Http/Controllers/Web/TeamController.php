<?php

namespace App\Http\Controllers\Web;

use App\Team;
use App\Referance;
use App\WhatWeDo;
use App\PostTeam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index(){

        $teams = Team::All();
        $referances = Referance::All();
        $whatwedoes = WhatWeDo::All();
        $postteams = PostTeam::All();

    	return view('web.pages.team', compact('teams','referances','whatwedoes','postteams'));
    }

    public function team($slug,$id){
    	$team = Team::where('slug', $slug)->first();
        $teams = Team::All();
        $whatwedoes = WhatWeDo::All();
        $postteams = PostTeam::All()->where('id', $id);

    	return view('web.team', compact('team','teams','whatwedoes','postteams'));
    }
}
