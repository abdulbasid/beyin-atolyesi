<?php

namespace App\Http\Controllers\Web;

use App\Article;
use App\Contact;
use App\Referance;
use App\WhatWeDo;
use App\Category;
use App\ArticleTag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){
        
        // $articles = Article::All();
        $contacts = Contact::All();
        $articles  = Article::orderBy('id', 'DESC')->paginate(3);
        $lastarticles  = Article::orderBy('id', 'DESC')->paginate(4);
        $whatwedoes = WhatWeDo::All();
        $categories = Category::All();
        $articletags = ArticleTag::All();

    	return view('web.pages.articles', compact('contacts','articles','lastarticles','whatwedoes','categories','articletags'));
    }
}
