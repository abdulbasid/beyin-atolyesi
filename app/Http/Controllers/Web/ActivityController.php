<?php

namespace App\Http\Controllers\Web;

use App\Post;
use App\Referance;
use App\Team;
use App\WhatWeDo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(){

        $referances = Referance::All();
        $teams = Team::All();
        $whatwedoes = WhatWeDo::All();
        $posts = Post::orderBy('id', 'DESC')->where('status', 'active')->paginate(3);
        $lastposts = Post::orderBy('id', 'DESC')->where('status', 'active')->paginate(4);

    	return view('web.pages.activity', compact('referances','teams','posts','lastposts','whatwedoes'));
    }

    public function post($slug){
        $post = Post::where('slug', $slug)->first();
        $tags = Tag::All();

    	return view('web.post', compact('post','tags'));
    }
}
