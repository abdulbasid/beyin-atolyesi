<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Tag;
use App\Post;
use App\Project;
use App\Slider;
use App\Contact;
use App\Referance;
use App\Team;
use App\Article;
use App\WhatWeDo;
use App\PostTeam;
use App\PostTag;
use App\RefCategory;
use App\ArticleTag;
use App\Partner;
use App\PartnerTag;
use App\RefTag;

class PageController extends Controller
{
    
    public function blog(){
        $posts = Post::orderBy('id', 'DESC')->where('status', 'active')->paginate(3);
        $homeposts = Post::orderBy('id', 'DESC')->paginate(8);
        $projects = Project::All();
        $services = Project::All();
        $sliders = Slider::All();
        $contacts = Contact::All();
        $referances = Referance::All();
        $teams = Team::All();
        $whatwedoes = WhatWeDo::All();

        $refcategories = RefCategory::All();
        $lastarticles  = Article::orderBy('id', 'DESC')->paginate(3);
        $homearticles  = Referance::orderBy('id', 'DESC')->where('status','active')->paginate(16);

    	return view('web.posts', compact('posts','projects','sliders','contacts','referances','teams','homeposts','lastarticles','whatwedoes','refcategories','homearticles'));
    }

    public function category($slug){
        $category = Category::where('slug', $slug)->pluck('id')->first();

        $posts = Post::where('category_id', $category)
            ->orderBy('id', 'DESC')->where('status', 'active')->paginate(3);

        return view('web.posts', compact('posts'));
    }

    public function tag($slug){ 
        $posts = Post::whereHas('tags', function($query) use ($slug) {
            $query->where('slug', $slug);
        })
        ->orderBy('id', 'DESC')->where('status', 'active')->paginate(3);

        return view('web.posts', compact('posts'));
    }

    public function post($slug){
        $post = Post::where('slug', $slug)->first();
        $tags = Tag::All();
        $lastposts = Post::orderBy('id', 'DESC')->where('status', 'active')->paginate(4);
        $whatwedoes = WhatWeDo::All();
        $posttags = PostTag::All();

    	return view('web.post', compact('post','tags','lastposts','whatwedoes','posttags'));
    }
    

    public function referance($slug){
    	$referance = Referance::where('slug', $slug)->first();
        $referances = Referance::All();
        $whatwedoes = WhatWeDo::All(); 
        $referancetags = RefTag::All();

    	return view('web.referance', compact('referance','referances','whatwedoes','referancetags'));
    }

    public function partner($slug){
    	$partner = Partner::where('slug', $slug)->first();
        $partners = Partner::All();
        $whatwedoes = WhatWeDo::All(); 
        $partnertags = PartnerTag::All();

    	return view('web.partner', compact('partner','partners','whatwedoes','partnertags'));
    }

    public function team($slug){
    	$team = Team::where('slug', $slug)->first();
        $teams = Team::All();
        $whatwedoes = WhatWeDo::All();
        $postteams = PostTeam::orderBy('id', 'DESC')->paginate(4);
        $posttags = PostTag::All();
        

    	return view('web.team', compact('team','teams','whatwedoes','postteams','posttags'));
    }

    public function article($slug){
    	$article = Article::where('slug', $slug)->first();
        $articles = Article::All();
        $lastarticles  = Article::orderBy('id', 'DESC')->paginate(4);
        $whatwedoes = WhatWeDo::All();
        $articletags = ArticleTag::All();

    	return view('web.article', compact('article','articles','lastarticles','whatwedoes','articletags'));
    }

    public function whatwedo($slug){
        $referances = Referance::All();
        $whatwedo = WhatWeDo::where('slug', $slug)->first();
        $whatwedoes = WhatWeDo::All();
       

    	return view('web.whatwedo', compact('referances','whatwedo','whatwedoes'));
    }

  

    public function project($slug){
    	$project = Project::where('slug', $slug)->first();
        $projects = Project::All();
        $contacts = Contact::All();
        $whatwedoes = WhatWeDo::All();

    	return view('web.project', compact('project','projects','contacts','whatwedoes'));
    }

}
