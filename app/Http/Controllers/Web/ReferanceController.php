<?php

namespace App\Http\Controllers\Web;

use App\Referance;
use App\Contact;
use App\WhatWeDo;
use App\RefCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferanceController extends Controller
{
  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function referance(){
        $referances = Referance::All();
        $refcategories = RefCategory::All();
        $contacts = Contact::All();
        $whatwedoes = WhatWeDo::All();

    	return view('web.pages.referances', compact('referances','contacts','refcategories','whatwedoes'));
    }
}
