<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostTeam extends Model
{
    protected $table = 'post_team';

    protected $fillable = [
        'post_id', 'team_id'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
