<?php

namespace App\Admin\Controllers;

use App\Team;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class TeamController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Team';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Team());

        $grid->column('id', __('Id'));
        $grid->column('body', __('Body'));
        $grid->column('name', __('Name'));
        $grid->column('email', __('Email'));
        $grid->column('phone', __('Phone'));
        $grid->column('job', __('Job'));
        $grid->column('unvan', __('Unvan'));
        $grid->column('city', __('City'));
        $grid->column('instagram', __('Instagram'));
        $grid->column('twitter', __('Twitter'));
        $grid->column('facebook', __('Facebook'));
        $grid->column('slug', __('Slug'));
        $grid->column('file', __('File'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->column('deleted_at', __('Deleted at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Team::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('body', __('Body'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('phone', __('Phone'));
        $show->field('job', __('Job'));
        $show->field('unvan', __('Unvan'));
        $show->field('city', __('City'));
        $show->field('instagram', __('Instagram'));
        $show->field('twitter', __('Twitter'));
        $show->field('facebook', __('Facebook'));
        $show->field('slug', __('Slug'));
        $show->field('file', __('File'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Team());

        $form->text('body', __('Body'));
        $form->text('name', __('Name'));
        $form->email('email', __('Email'));
        $form->mobile('phone', __('Phone'));
        $form->text('job', __('Job'));
        $form->text('unvan', __('Unvan'));
        $form->text('city', __('City'));
        $form->text('instagram', __('Instagram'));
        $form->text('twitter', __('Twitter'));
        $form->text('facebook', __('Facebook'));
        $form->text('slug', __('Slug'));
        $form->file('file', __('File'));

        return $form;
    }
}
