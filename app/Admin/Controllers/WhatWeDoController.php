<?php

namespace App\Admin\Controllers;

use App\WhatWeDo;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class WhatWeDoController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\WhatWeDo';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WhatWeDo());

        $grid->column('id', __('Id'));
        $grid->column('main_title', __('Main title'));
        $grid->column('slug', __('Slug'));
        $grid->column('title', __('Title'));
        $grid->column('body', __('Body'));
        $grid->column('file', __('File'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->column('deleted_at', __('Deleted at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WhatWeDo::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('main_title', __('Main title'));
        $show->field('slug', __('Slug'));
        $show->field('title', __('Title'));
        $show->field('body', __('Body'));
        $show->field('file', __('File'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WhatWeDo());

        $form->text('main_title', __('Main title'));
        $form->text('slug', __('Slug'));
        $form->text('title', __('Title'));
        $form->text('body', __('Body'));
        $form->file('file', __('File'));

        return $form;
    }
}
