<?php

namespace App\Admin\Controllers;

use App\Contact;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ContactController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Contact';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Contact());

        $grid->column('id', __('Id'));
        $grid->column('address', __('Address'));
        $grid->column('email', __('Email'));
        $grid->column('gsm', __('Gsm'));
        $grid->column('gsm2', __('Gsm2'));
        $grid->column('map', __('Map'));
        $grid->column('file', __('File'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->column('deleted_at', __('Deleted at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Contact::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('address', __('Address'));
        $show->field('email', __('Email'));
        $show->field('gsm', __('Gsm'));
        $show->field('gsm2', __('Gsm2'));
        $show->field('map', __('Map'));
        $show->field('file', __('File'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('deleted_at', __('Deleted at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Contact());

        $form->text('address', __('Address'));
        $form->email('email', __('Email'));
        $form->text('gsm', __('Gsm'));
        $form->text('gsm2', __('Gsm2'));
        $form->text('map', __('Map'));
        $form->file('file', __('File'));

        return $form;
    }
}
