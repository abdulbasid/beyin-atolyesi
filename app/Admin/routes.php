<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');

    $router->resource('/aboutuses', AboutusController::class); // Hakkımızda
    $router->resource('/articless', ArticleController::class); // Makaleler
    $router->resource('/refcategories', CategoryController::class); // Kategoriler
    $router->resource('/contacts', ContactController::class); // İletişim
    $router->resource('/contact_messages', ContactMessageController::class); // Gelen Mesajlar
    $router->resource('/partners', PartnerController::class); // Etkinlikler
    $router->resource('/referances', ReferanceController::class); // Referanslar
    $router->resource('/sliders', SliderController::class); // Slider
    $router->resource('/teams', TeamController::class); // Ekip
    $router->resource('/settings', SettingController::class); // Ayarlar

});
