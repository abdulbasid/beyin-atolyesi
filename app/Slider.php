<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'type' ,'name','body','button', 'slug', 'link' ,'file'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
