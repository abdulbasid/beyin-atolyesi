<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = 'partners';
    protected $fillable = [
       'body','name','slug','status','file'
    ];

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}

