<?php

namespace App\Providers;

use App\Post;
use App\Project;
use App\Slider;
use App\Aboutus;
use App\Contact;
use App\Referance;
use App\Team;
use App\Article;
use App\RefCategory;
use App\WhatWeDo;
use App\HowDo;
use App\Partner;
use App\Policies\PostPolicy;
use App\Policies\ProjectPolicy;
use App\Policies\SliderPolicy;
use App\Policies\AboutusPolicy;
use App\Policies\ContactPolicy;
use App\Policies\ReferancePolicy;
use App\Policies\TeamPolicy;
use App\Policies\ArticlePolicy;
use App\Policies\WhatWeDoPolicy;
use App\Policies\HowDoPolicy;
use App\Policies\PartnerPolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Post::class => PostPolicy::class,
        Project::class => ProjectPolicy::class,
        Slider::class => SliderPolicy::class,
        Aboutus::class => AboutusPolicy::class,
        Contact::class => ContactPolicy::class,
        Referance::class => ReferancePolicy::class,
        Team::class =>TeamPolicy::class,
        Article::class =>ArticlePolicy::class,
        WhatWeDo::class =>WhatWeDoPolicy::class,
        RefCategory::class =>RefCategoryPolicy::class,
        HowDo::class =>HowDoPolicy::class,
        Partner::class =>PartnerPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
