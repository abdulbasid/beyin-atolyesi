<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HowDo extends Model

{

    protected $table = 'howdoes';

    protected $fillable = [
         'title','body','slug','file'
    ];


  
}
