<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
         'body','name','email','phone','job','unvan','city','instagram','twitter','facebook','slug','file'
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
