<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('welcome');
});

Auth::routes();


Route::get('/welcome', 'Web\PageController@blog')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/project/{slug}', 'Web\PageController@project')->name('project');
Route::get('/referance/{slug}', 'Web\PageController@referance')->name('referance');
Route::get('/partner/{slug}', 'Web\PageController@partner')->name('partner');
Route::get('/whatwedo/{slug}', 'Web\PageController@whatwedo')->name('whatwedo');
Route::get('/team/{slug}', 'Web\PageController@team')->name('team');
Route::get('/slider/{slug}', 'Web\PageController@slider')->name('slider');
Route::get('/post/{slug}', 'Web\PageController@post')->name('post');
Route::get('/category/{slug}', 'Web\PageController@category')->name('category');
Route::get('/tag/{slug}', 'Web\PageController@tag')->name('tag');
Route::get('/article/{slug}', 'Web\PageController@article')->name('article');


Route::resource('tags','Admin\TagController');
Route::resource('categories','Admin\CategoryController');
Route::resource('refcategories','Admin\RefCategoryController');
Route::resource('posts','Admin\PostController');
Route::resource('projects','Admin\ProjectController');
Route::resource('sliders','Admin\SliderController');
Route::resource('aboutuses','Admin\AboutusController');
Route::resource('contacts','Admin\ContactController');
Route::resource('referanslar','Admin\ReferanceController');
Route::resource('teams','Admin\TeamController');
Route::resource('calender','Admin\CalenderController');
Route::resource('articless','Admin\ArticleController');
Route::resource('whatwedoess','Admin\WhatWeDoController');
Route::resource('howdoes','Admin\HowDoController');
Route::resource('partners','Admin\PartnerController');


Route::name('web.pages.')->group(function () {
    Route::get('referances', 'Web\ReferanceController@referance')->name('referances');
    Route::get('partnerss', 'Web\PartnerController@index')->name('partnerss');
    Route::get('about-us', 'Web\AboutUsController@index')->name('about-us');
    Route::get('team', 'Web\TeamController@index')->name('team');
    Route::get('contact', 'Web\ContactController@index')->name('contact');
    Route::get('activity', 'Web\ActivityController@index')->name('activity');
    Route::get('articles', 'Web\ArticleController@index')->name('articles');
    Route::get('whatwedoes', 'Web\WhaWeDoController@index')->name('whatwedoes');
});

Route::get('twitterUserTimeLine', 'TwitterController@twitterUserTimeLine');
Route::post('tweet', ['as'=>'post.tweet','uses'=>'TwitterController@tweet']);


Route::get('events', 'EventController@index');

Route::namespace('Auth')->group(function () {
    Route::get('logout', 'LoginController@logout');
});